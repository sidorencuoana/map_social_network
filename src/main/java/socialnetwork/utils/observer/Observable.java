package socialnetwork.utils.observer;

import socialnetwork.utils.events.TaskChangeEvent;

public interface Observable <E extends TaskChangeEvent>{
    void addObserver(Observer<E> e);
    void notifyObservers(E t);

}
