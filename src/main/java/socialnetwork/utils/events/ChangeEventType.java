package socialnetwork.utils.events;

public enum ChangeEventType {
    FRIEND_REQUEST,DELETE_FRIEND,DELETE_FRIEND_REQUEST,ADD_FRIEND,
    CREATE_GROUP
    ,SEND_PRIVATE_MESSAGE,
    ADD_EVENT,
    JOIN_EVENT,
    NOTIFY,
    LEAVE_EVENT;

}
