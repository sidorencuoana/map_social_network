package socialnetwork.utils.events;

public class TaskChangeEvent implements Event {
    private ChangeEventType type;

    public TaskChangeEvent(ChangeEventType type) {
        this.type = type;

    }

    public ChangeEventType getType() {
        return type;
    }

}