package socialnetwork.domain;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    @Override
    public String toString() {
        return super.toString() +
                "date=" + date;
    }

    public Prietenie(LocalDateTime data){
        date = data;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }
}
