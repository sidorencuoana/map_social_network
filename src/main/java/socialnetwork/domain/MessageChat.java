package socialnetwork.domain;

public class MessageChat {
    private Long id_message;
    private Long from;
    private String continut;

    public MessageChat(Long id_message, Long from, String continut) {
        this.id_message = id_message;
        this.from = from;
        this.continut = continut;
    }

    public Long getId_message() {
        return id_message;
    }

    public void setId_message(Long id_message) {
        this.id_message = id_message;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }
}
