package socialnetwork.domain.validators;

import socialnetwork.domain.Event;

public class EventValidator implements Validator<Event> {
    @Override
    public void validate(Event entity) throws ValidationException {
        int error=0;
        if(entity.getDescriere().length()<5){
            error++;
        }
        if(entity.getNume().length()<5){
            error++;
        }
        if(entity.getData_end().toString().compareTo(entity.getData_start().toString())<0){
            error++;
        }
        if(error!=0){
            throw new ValidationException("Event invalid!");
        }


    }
}
