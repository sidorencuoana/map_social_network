package socialnetwork.domain.validators;

public interface Validator<T> {
    /**
     * validates an entity
     * @param entity
     * @throws ValidationException
     */
    void validate(T entity) throws ValidationException;
}