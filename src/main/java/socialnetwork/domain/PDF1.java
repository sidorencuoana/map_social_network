package socialnetwork.domain;

public class PDF1 {
    private String nume;
    private String data;

    public PDF1(String nume, String data) {
        this.nume = nume;
        this.data = data;
    }

    public String getNume() {
        return nume;
    }

    public String getData() {
        return data;
    }
}
