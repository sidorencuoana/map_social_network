package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRequest extends Entity<Tuple<Long,Long>> {
    private String status;
    LocalDateTime data_time;

    public FriendRequest(String status) {
        this.status = status;
    }

    public FriendRequest(String status, LocalDateTime data_time) {
        this.status = status;
        this.data_time = data_time;
    }

    public LocalDateTime getData_time() {
        return data_time;
    }

    public void setData_time(LocalDateTime data_time) {
        this.data_time = data_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
