package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class Message extends Entity<Long>{
    private Long id_msg;
    private Long from;
    private Long to;
    private String message;
    private LocalDateTime data;
    private Long reply_message;
    private Long id_grup;

    public Message(Long id_msg,Long from, String message, Long to,LocalDateTime data, Long reply_message) {
        this.id_msg = id_msg;
        this.from = from;
        this.message = message;
        this.data = data;
        this.reply_message = reply_message;
        this.to = to;
    }

    public Message(Long from, String message, Long to,LocalDateTime data, Long reply_message,Long id_grup) {
        this.from = from;
        this.message = message;
        this.data = data;
        this.reply_message = reply_message;
        this.to = to;
        this.id_grup=id_grup;
    }

    public Message(Long id_msg, Long from, Long to, String message, LocalDateTime data, Long reply_message, Long id_grup) {
        this.id_msg = id_msg;
        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        this.reply_message = reply_message;
        this.id_grup = id_grup;
    }

    public Message(Long from, String message, Long to, LocalDateTime data, Long reply_message) {
        this.from = from;
        this.message = message;
        this.data = data;
        this.reply_message = reply_message;
        this.to = to;
    }



    public Long getId_msg() {
        return id_msg;
    }

    public void setId_msg(Long id_msg) {
        this.id_msg = id_msg;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Long getId_grup() {
        return id_grup;
    }

    public void setId_grup(Long id_grup) {
        this.id_grup = id_grup;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Long getReply_message() {
        return reply_message;
    }

    public void setReply_message(Long reply_message) {
        this.reply_message = reply_message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return getFrom().equals(message1.getFrom()) &&
                getTo().equals(message1.getTo()) &&
                getMessage().equals(message1.getMessage()) &&
                getData().equals(message1.getData()) &&
                getReply_message().equals(message1.getReply_message());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), getMessage(), getData(), getReply_message());
    }

    @Override
    public String toString() {
        return "Message{" +
                ", message='" + message + '\n' +
                ", data=" + data + '\n' +
                ", reply_message=" + reply_message + '\n' +
                '}';
    }
}
