package socialnetwork.domain;

import java.sql.Time;
import java.time.LocalDateTime;

public class Event {
    private Long id;
    private String nume;
    private String descriere;
    private LocalDateTime data_start;
    private LocalDateTime data_end;
    private Time time_start;
    private Time time_end;
    private Long notify;

    public Event(Long id, String nume, String descriere, LocalDateTime data_start, LocalDateTime data_end) {
        this.id = id;
        this.nume = nume;
        this.descriere = descriere;
        this.data_start = data_start;
        this.data_end = data_end;

    }

    public Event(Long id, String nume, String descriere, LocalDateTime data_start, LocalDateTime data_end, Time time_start, Time time_end) {
        this.id = id;
        this.nume = nume;
        this.descriere = descriere;
        this.data_start = data_start;
        this.data_end = data_end;
        this.time_start = time_start;
        this.time_end = time_end;
    }

    public Event(String nume, String descriere, LocalDateTime data_start, LocalDateTime data_end, Time time_start, Time time_end) {
        this.nume = nume;
        this.descriere = descriere;
        this.data_start = data_start;
        this.data_end = data_end;
        this.time_start = time_start;
        this.time_end = time_end;
    }

    public Event(Long id, String nume, String descriere, LocalDateTime data_start, LocalDateTime data_end, Time time_start, Time time_end, Long notify) {
        this.id = id;
        this.nume = nume;
        this.descriere = descriere;
        this.data_start = data_start;
        this.data_end = data_end;
        this.time_start = time_start;
        this.time_end = time_end;
        this.notify = notify;
    }

    public Long getId() {
        return id;
    }

    public String getNume() {
        return nume;
    }

    public String getDescriere() {
        return descriere;
    }

    public LocalDateTime getData_start() {
        return data_start;
    }

    public LocalDateTime getData_end() {
        return data_end;
    }

    public Time getTime_start() {
        return time_start;
    }

    public Time getTime_end() {
        return time_end;
    }

    public Long getNotify() {
        return notify;
    }

    public void setNotify(Long notify) {
        this.notify = notify;
    }
}
