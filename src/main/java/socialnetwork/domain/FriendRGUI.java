package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRGUI {
    private String nume;
    private String prenume;
    private String status;
    LocalDateTime data;
    private Long id;

    public FriendRGUI(String nume, String prenume, String status, LocalDateTime data,Long id) {
        this.nume = nume;
        this.prenume = prenume;
        this.status = status;
        this.data = data;
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
