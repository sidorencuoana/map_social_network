package socialnetwork.domain;

public class ChatUsersGUI {
    private Long id;
    private String nume;

    public ChatUsersGUI(Long id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}
