package socialnetwork.domain;

public class UserChat {
    private String nume;
    private Long id;

    public UserChat(String nume, Long id) {
        this.nume = nume;
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
