package socialnetwork.pdf;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.ObservableList;
import socialnetwork.domain.PDF1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PDF{
    public void generare_pdf(String numepdf,ObservableList<PDF1> list, ObservableList<PDF1> list2)  {
        Document document = new Document();
        try{
            PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(numepdf));
            document.open();
            if(list.size() == 0 && list2.size() == 0) {
                Paragraph par = new Paragraph("Nu exista activitate in perioada selectata!");
                document.add(par);
            }
            else {
                Paragraph par = new Paragraph("Prieteni noi: \n\n");

                PdfPTable table = new PdfPTable(2);


                for (PDF1 elem : list) {
                    table.addCell(elem.getNume());
                    table.addCell(elem.getData());
                }

                Paragraph par2 = new Paragraph("Mesaje primite: \n\n");
                 PdfPTable table2 = new PdfPTable(2);
                for (PDF1 elem : list2) {
                    table2.addCell(elem.getNume());
                    table2.addCell(elem.getData());
                }
                document.add(par);
                document.add(table);
                document.add(par2);
                document.add(table2);
            }
            document.close();
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }


    }

    public void generare_pdf2(String numepdf,ObservableList<PDF1> list)  {
        Document document = new Document();
        try{
            PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(numepdf));
            document.open();
            if(list.size() == 0) {
                Paragraph par = new Paragraph("Nu exista mesaje primite de la utilizatorul selectat in perioada selectata!");
                document.add(par);
            }
            else {
                PdfPTable table = new PdfPTable(2);

                Paragraph par = new Paragraph("Mesaje primite: \n\n");
                document.add(par);

                for (PDF1 elem : list) {
                    table.addCell(elem.getNume());
                    table.addCell(elem.getData());
                }


                document.add(table);
            }
            document.close();
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }


    }
}
