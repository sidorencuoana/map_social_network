package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;
import socialnetwork.service.PageUser;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class HomePageController{
    BDService service;

    @FXML
    private TextField idField;
    @FXML
    private PasswordField passwordField;

    public void setService(BDService service) {
        this.service = service;
    }

    public void handleCreateAccount(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/createaccountPage.fxml"));
        Parent createaccountParent = loader.load();

        CreateAccountController controller  = loader.getController();
        controller.setService(service);

        Scene createaccountScene = new Scene(createaccountParent,700,500);
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();

        window.setScene(createaccountScene);
        window.show();
    }


    public void handleSingIn(ActionEvent event) throws IOException {
        try{
            String nr = idField.getText();
            Long id = Long.parseLong(nr);
            String password = passwordField.getText();
            if(nr.equals("") || password.equals(""))
                throw new ValidationException("Invalid!");
            Utilizator user = service.find_user(id);
            String key = "Bar12345Bar12345";
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(password.getBytes());
            String pp = new String(encrypted);
            if(!user.getPassword().equals(pp))
                throw new ValidationException("Parola incorecta!");
            MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Accept", "User valid");
            clearFields();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/homeUserPage.fxml"));
            Parent createaccountParent = loader.load();

            PageUser page_user = new PageUser(user.getId(), user.getFirstName(), user.getLastName(), service.get_all_friends(user.getId()),service.get_all_friendrequest(user.getId()),service.get_all_message_user(user.getId()),service.get_all_message_grup(user.getId()),service.get_all_my_events(user.getId()));
            HomeUserController controller  = loader.getController();
            controller.setHomeUserService(service,user,page_user);

            Scene createaccountScene = new Scene(createaccountParent,700,500);
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();

            window.setScene(createaccountScene);
            window.show();
        }catch (ValidationException e) {
            MessageAlert.showErrorMessage(null,e.getMessage());
        }catch (NumberFormatException e) {
            MessageAlert.showErrorMessage(null,"Invalid!");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    void clearFields() {
        idField.setText("");
        passwordField.setText("");
    }
}
