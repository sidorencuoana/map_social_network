package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;

import java.util.ArrayList;
import java.util.List;

public class CreateGroupController {
    BDService service;
    Utilizator user;
    Long count1;
    Long MAX1;


    @FXML
    private TableView<Utilizator> tableUsers;

    @FXML
    private TableColumn<Utilizator, String> tableColumnName1;

    @FXML
    private TableColumn<Utilizator, String> tableColumnPrenume1;

    @FXML
    private TableView<Utilizator> tableUsersGroup;

    @FXML
    private TableColumn<Utilizator, String> tableColumnName2;

    @FXML
    private TableColumn<Utilizator, String> tableColumnPrenume2;

    @FXML
    private ImageView RightImage;

    @FXML
    private ImageView leftImage;

    @FXML
    private ImageView AddImage;

    @FXML
    private ImageView RemoveImage;

    @FXML
    private Label countField;

    ObservableList<Utilizator> modelUsers = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelUsersGroup = FXCollections.observableArrayList();

    public void setController(BDService service,Utilizator user) {
        this.service = service;
        this.user =user;


        count1 = 1L;
        countField.setText(count1.toString());
        Long size = service.number_user();
        MAX1 = size / 6;
        if (size % 6 != 0)
            MAX1++;

        initTableUsers();
        modelUsersGroup.add(user);

    }

    @FXML
    public void initialize() {
        tableUsers.setItems(modelUsers);
        tableUsersGroup.setItems(modelUsersGroup);

        tableColumnName1.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnPrenume1.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnName2.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnPrenume2.setCellValueFactory(new PropertyValueFactory<>("lastName"));


    }

    public void handleRight1Button(MouseEvent mouseEvent) {
        if (count1 + 1 <= MAX1) {
            count1++;
            countField.setText(count1.toString());
            initTableUsers();
        }
    }

    public void handleLeft1Button(MouseEvent mouseEvent) {
        if (count1 - 1 >= 1) {
            count1--;
            countField.setText(count1.toString());
            initTableUsers();
        }
    }

    public void handleRightEntered(MouseEvent mouseEvent) {
        RightImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px.png"))));

    }

    public void handleRightExited(MouseEvent mouseEvent) {
        RightImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px_3.png"))));
    }

    public void handleLeftEntered(MouseEvent mouseEvent) {
        leftImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px.png"))));

    }

    public void handleLeftExited(MouseEvent mouseEvent) {
        leftImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px_2.png"))));
    }


    @FXML
    void addEntered(MouseEvent event) {
        AddImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_add_24px_3.png"))));


    }

    @FXML
    void addExited(MouseEvent event) {
        AddImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_add_32px.png"))));

    }


    @FXML
    void removeEntered(MouseEvent event) {
        RemoveImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_cancel_32px_1.png"))));
    }

    @FXML
    void removeExited(MouseEvent event) {
        RemoveImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_cancel_32px.png"))));

    }


    public void initTableUsers() {
        String nume = "";
        modelUsers.setAll(service.get_all_users(count1,nume));
    }

    public void handleaddUser(MouseEvent mouseEvent) {
        Utilizator userr = tableUsers.getSelectionModel().getSelectedItem();
        if(!modelUsersGroup.contains(userr) && userr!=null){
            modelUsersGroup.add(userr);
        }
    }

    public void handledeleteUser(MouseEvent mouseEvent) {
        Utilizator userr = tableUsersGroup.getSelectionModel().getSelectedItem();
        if(userr == null)
        {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un user!");
        }
        else
        if(!userr.equals(user)) {
            modelUsersGroup.remove(userr);
        }
    }

    public void handleUserGroupSelectat(MouseEvent mouseEvent) {
        tableUsers.getSelectionModel().clearSelection();
    }

    public void handleUserSeelctat(MouseEvent mouseEvent) {
        tableUsersGroup.getSelectionModel().clearSelection();
    }

    @FXML
    public TextField nameLabel;
    public void handleCreateGroup(ActionEvent actionEvent) {
        List<Utilizator> list = tableUsersGroup.getItems();
        String nume = nameLabel.getText();
        if (list.size() <= 1) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi 2 sau mai multi utilizatori!");
        } else if (nume.equals("")) {
            MessageAlert.showErrorMessage(null, "Trebuie sa introduci un nume grupului");

        } else {
            try {
                List<Long> list_user = new ArrayList<>();
                for (Utilizator userr : list) {
                    list_user.add(userr.getId());
                }
                service.create_group(nume, list_user);
                nameLabel.setText("");
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }
}
