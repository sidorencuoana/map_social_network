package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class CreateAccountController{
    BDService service;
    private static String secretKey = "boooooooooom!!!!";
    private static String salt = "ssshhhhhhhhhhh!!!!";


    public void setService(BDService service) {
        this.service = service;
    }

    @FXML
    private TextField numeField;

    @FXML
    private TextField prenumeField;

    @FXML
    private TextField password1Field;

    @FXML
    private TextField password2Field;


    public void handlebackbutton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/homePage.fxml"));
        Parent createaccountParent = loader.load();

        HomePageController controller  = loader.getController();
        controller.setService(service);

        Scene createaccountScene = new Scene(createaccountParent,700,500);
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();

        window.setScene(createaccountScene);
        window.show();
    }



     @FXML
    public void handleCreateAccount() throws NoSuchAlgorithmException {


        String nume = numeField.getText();
        String prenume = prenumeField.getText();
        String password1 = password1Field.getText();
        String password2 = password2Field.getText();
        try {
            if(nume.equals("") || prenume.equals("")|| password1.equals("") || password2.equals(""))
                throw new ValidationException ("Toate campurile sunt obligatorii");
            if(!password1.equals(password2))
                throw new ValidationException ("Parola invalida");


            String key = "Bar12345Bar12345";
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(password1.getBytes());
            String pp = new String(encrypted);



            Utilizator user = new Utilizator(nume,prenume,pp);
            service.add_user(user);
            MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Create Acount OK", "User added");
            clearFields();
        }catch (ValidationException e) {
            MessageAlert.showErrorMessage(null,e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

     }

    private void clearFields() {
        numeField.setText("");
        prenumeField.setText("");
        password1Field.setText("");
        password2Field.setText("");
    }
}
