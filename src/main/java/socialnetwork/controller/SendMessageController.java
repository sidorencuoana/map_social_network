package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;
import socialnetwork.service.PageUser;

public class SendMessageController {
    BDService service;
    Utilizator user_from;
    Utilizator user_to;
    PageUser pageUser;


    @FXML
    private Label NameLabel;

    @FXML
    private TextArea messageTextArea;

    public void setController(BDService service,Utilizator user_from,Utilizator user_to,PageUser pageUser)
    {
        this.service=service;
        this.user_from=user_from;
        this.user_to=user_to;
        this.pageUser = pageUser;

        NameLabel.setText(new String(user_to.getFirstName()+" "+user_to.getLastName()));
    }

    @FXML
    public void initialize()
    {

    }


    @FXML
    void handleSendMessage(ActionEvent event) {
        String text = messageTextArea.getText();
        if (text.equals("")) {
            MessageAlert.showErrorMessage(null, "Trebuie sa introduci un mesaj");
        } else {
            try {
                Long iddd = service.add_message(user_from.getId(), text, user_to.getId(), (long) 0, null);
                pageUser.add_message(iddd, user_from.getId(), text, user_to.getId(), (long) 0, null);
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Mesaj trimis!");
                messageTextArea.setText("");
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

}
