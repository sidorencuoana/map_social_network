package socialnetwork.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Event;
import socialnetwork.service.BDService;
import socialnetwork.service.PageUser;

public class DetailsEventController {

    private Event event;

    private BDService service;

    private PageUser pageUser;

    public Label labelName;

    @FXML
    private TextArea descriptionField;

    public Label lableStartDate;

    public Label labelTimeStart;

    public Label labelEndDate;

    public Label labelTimeEnd;

    @FXML
    public Button notButton;
    @FXML
    public Button notifyButton;
    @FXML
    public Button joinButton;
    @FXML
    public Button LeaveButton;

    public ImageView NotifyImage;
    public ImageView NotNotifyImage;


    public void setController(Event event, BDService service,PageUser pageUser) {
        this.event = event;
        this.service = service;
        this.pageUser = pageUser;
        labelName.setText(event.getNume());
        descriptionField.setText(event.getDescriere());
        lableStartDate.setText(event.getData_start().toString());
        labelEndDate.setText(event.getData_end().toString());
        labelTimeStart.setText(event.getTime_start().toString());
        labelTimeEnd.setText(event.getTime_end().toString());
        if(pageUser.user_particip_event(event.getId()) == true){
            joinButton.setVisible(false);
            LeaveButton.setVisible(true);
            if(pageUser.stare_user_event(event.getId()) == true){
                notButton.setVisible(true);
                notifyButton.setVisible(false);
            }
            else
            {
                notButton.setVisible(false);
                notifyButton.setVisible(true);
            }

        }
        else
        {
            LeaveButton.setVisible(false);
            joinButton.setVisible(true);
            notButton.setVisible(false);
            notifyButton.setVisible(false);
        }

    }

    public void handleNootify(ActionEvent actionEvent) {
//        try{
//            if(pageUser.user_participa_la_event_notifica_0(event.getId()) == true){
//                service.update_notify(pageUser.getId(), event.getId(), (long) 1);
//                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Notificarile de la acest event o sa  fie activate!");
//                notifyButton.setVisible(false);
//                notButton.setVisible(true);
//            }
//            else
//            {
//                throw new ValidationException("Nu esti abonat la eventul selectat!");
//            }
//        }catch (ValidationException e) {
//            MessageAlert.showErrorMessage(null, e.getMessage());
//        }
        pageUser.user_participa_la_event_notifica_0(event.getId());
        service.update_notify(pageUser.getId(), event.getId(), (long) 1);
        notifyButton.setVisible(false);
        notButton.setVisible(true);


    }

    public void handleNotNotify(ActionEvent actionEvent) {
//        try{
//            if(pageUser.user_participa_la_event_notifica_1(event.getId())==true){
//                service.update_notify(pageUser.getId(), event.getId(), (long) 0);
//                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Notificarile de la acest event o sa  fie dezactivate!");
//                notifyButton.setVisible(true);
//                notButton.setVisible(false);
//            }
//            else
//            {
//                throw new ValidationException("Nu esti abonat la eventul selectat!");
//            }
//        }catch (ValidationException e) {
//            MessageAlert.showErrorMessage(null, e.getMessage());
//        }
        pageUser.user_participa_la_event_notifica_1(event.getId());
        service.update_notify(pageUser.getId(), event.getId(), (long) 0);
        notifyButton.setVisible(true);
        notButton.setVisible(false);

    }

    public void handleJoinEvent(ActionEvent actionEvent) {
//        try{
//            if(pageUser.user_join_event(event) == true)
//            {
//                service.user_join_event(event.getId(),pageUser.getId());
//                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Join la event!");
//            }
//            else
//            {
//                throw new ValidationException("Esti deja abonat la acest event!");
//            }
//        }catch (ValidationException e) {
//            MessageAlert.showErrorMessage(null, e.getMessage());
//        }
        pageUser.user_join_event(event);
        service.user_join_event(event.getId(),pageUser.getId());
        joinButton.setVisible(false);
        notButton.setVisible(true);
        notifyButton.setVisible(false);
        LeaveButton.setVisible(true);


    }

    public void handleLeaveEvent(ActionEvent actionEvent) {
        service.user_leave_event(event.getId(),pageUser.getId());
        pageUser.user_leave_event(event,service.get_all_my_events(pageUser.getId()));
        joinButton.setVisible(true);
        LeaveButton.setVisible(false);
        notButton.setVisible(false);
        notifyButton.setVisible(false);


    }

    public void NotifyEntered(MouseEvent mouseEvent) {
        NotifyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_sound_50px.png"))));
    }

    public void NotifyExited(MouseEvent mouseEvent) {
        NotifyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_audio_50px.png"))));
    }

    public void NotNotifyEntered(MouseEvent mouseEvent) {
        NotNotifyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_mute_50px.png"))));
    }

    public void NotNotifyExited(MouseEvent mouseEvent) {
        NotNotifyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_mute_50px_1.png"))));
    }
}
