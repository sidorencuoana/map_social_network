package socialnetwork.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.pdf.PDF;
import socialnetwork.service.BDService;
import socialnetwork.service.PageUser;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.TaskChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class HomeUserController implements Observer<TaskChangeEvent> {
    BDService service;
    Utilizator user;
    PageUser page_User;
    Long count1;
    Long MAX1;
    Long count2;
    Long MAX2;
    Long countME;
    Long MAXME;
    Long MAXChat;
    Long countChat;
    //si aici
    //o lista obs de userri
    ObservableList<Utilizator> modelGrade = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelFriend = FXCollections.observableArrayList();
    ObservableList<FriendRGUI> modelFriendRequest = FXCollections.observableArrayList();
    ObservableList<UserChat> modelUserChat = FXCollections.observableArrayList();
    ObservableList<MessageChat> modelChat = FXCollections.observableArrayList();
    ObservableList<Group> modelGroup = FXCollections.observableArrayList();
    ObservableList<PDF1> modelpdf = FXCollections.observableArrayList();
    ObservableList<PDF1> modelpdf2 = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelGrade1 = FXCollections.observableArrayList();
    ObservableList<PDF1> modelpdf3 = FXCollections.observableArrayList();
    ObservableList<Event> modelEvent = FXCollections.observableArrayList();
    ObservableList<Event> modelUserEvent = FXCollections.observableArrayList();
    ObservableList<Event> modelMyEvents = FXCollections.observableArrayList();

    @FXML
    TableView<Utilizator> tableUsers;
    @FXML
    TableColumn<Utilizator, String> tableColumnName;
    @FXML
    TableColumn<Utilizator, String> tableColumnPrenume;
    @FXML
    TextArea sendmessage1TextArea;
    @FXML
    TextField serchField;
    @FXML
    TextField numeGrupField;

    @FXML
    private ImageView createGroupImage;

    @FXML
    TableView<Utilizator> tableFriends;
    @FXML
    TableColumn<Prietenie, String> tableColumnFName;
    @FXML
    TableColumn<Prietenie, String> tableColumnFPrenume;

    @FXML
    TableView<FriendRGUI> tableFriendsRequest;
    @FXML
    TableColumn<FriendRGUI, String> tableColumFRName;
    @FXML
    TableColumn<FriendRGUI, String> tableColumFRPrenume;
    @FXML
    TableColumn<FriendRGUI, String> tableColumFRStatus;
    @FXML
    TableColumn<FriendRGUI, LocalDateTime> tableColumFRData;


    @FXML
    TableView<UserChat> tableUsersChat;
    @FXML
    TableColumn<UserChat, String> tableColumnUCName;

    @FXML
    TableView<MessageChat> tableChat;
    @FXML
    TableColumn<MessageChat, String> tableColumnChat;

    @FXML
    TextArea messageTextArea;

    @FXML
    TableView<Group> tableGroups;
    @FXML
    TableColumn<Group, String> tableColumnGroups;

    @FXML
    DatePicker data_pdf1_start;
    @FXML
    DatePicker data_pdf1_end;
    @FXML
    TableView<PDF1> tablepdf1_1;
    @FXML
    TableColumn<Utilizator, String> tableColumn1pdf1_1;
    @FXML
    TableColumn<Utilizator, String> tableColumn2pdf1_1;

    @FXML
    TableView<PDF1> tablepdf1_2;
    @FXML
    TableColumn<Utilizator, String> tableColumn1pdf1_2;
    @FXML
    TableColumn<Utilizator, String> tableColumn2pdf1_2;

    @FXML
    TableView<Utilizator> tableUsers1;
    @FXML
    TableColumn<Utilizator, String> tableColumnName1;
    @FXML
    TableColumn<Utilizator, String> tableColumnPrenume1;
    @FXML
    TextField serchField1;

    @FXML
    DatePicker data_pdf2_start;
    @FXML
    DatePicker data_pdf2_end;

    @FXML
    TableView<PDF1> tablepdf2;
    @FXML
    TableColumn<Utilizator, String> tableColumn1pdf2;
    @FXML
    TableColumn<Utilizator, String> tableColumn2pdf2;

    @FXML
    private TableView<Event> tableEvent;

    @FXML
    private TableColumn<Event, String> tableColumnEName;

    @FXML
    private TableColumn<Event, LocalDateTime> tableColumnEStart;

    @FXML
    private TableColumn<Event, LocalDateTime> tableColumnEEnd;

    @FXML
    private TableView<Event> tableUserEvent;

    @FXML
    private TableColumn<Event, String> tableColumnUEName;

    @FXML
    private TableColumn<Event, LocalDateTime> tableColumnUEStart;

    @FXML
    private TableColumn<Event, LocalDateTime> tableColumnUEEnd;

    @FXML
    public ImageView Right1Image;
    @FXML
    public ImageView Left1Image;
    @FXML
    public Label Count1Label;
    @FXML
    public ImageView sendMessage1Image;
    @FXML
    public ImageView addFriendImage;
    @FXML
    public ImageView deleteFRImage;
    @FXML
    public ImageView Right2Image;
    @FXML
    public ImageView Left2Image;
    @FXML
    public Label Count2Label;

    @FXML
    public ImageView RightMEImage;
    @FXML
    public ImageView LeftMEImage;
    @FXML
    public Label CountMELabel;

    @FXML
    private Label idLabel;
    @FXML
    private Label FirstNameLabel;
    @FXML
    private Label LastNameLabel;


    private final ScheduledExecutorService thread = Executors.newScheduledThreadPool(1);




    @FXML
    public void initialize() {
        //legam observablelist de tableUsers
        tableUsers.setItems(modelGrade);
        tableFriends.setItems(modelFriend);
        tableFriendsRequest.setItems(modelFriendRequest);
        tableUsersChat.setItems(modelUserChat);
        tableChat.setItems(modelChat);
        tableGroups.setItems(modelGroup);
        tableUsers1.setItems(modelGrade1);
        tablepdf1_1.setItems(modelpdf);
        tablepdf1_2.setItems(modelpdf2);
        tablepdf2.setItems(modelpdf3);
        tableEvent.setItems(modelEvent);
        tableUserEvent.setItems(modelUserEvent);

        SendFriendRequest.setVisible(false);
        DeleteFriendRequest.setVisible(false);

        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnPrenume.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnFName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnFPrenume.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumFRName.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumFRPrenume.setCellValueFactory(new PropertyValueFactory<>("prenume"));
        tableColumFRStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        tableColumFRData.setCellValueFactory(new PropertyValueFactory<>("data"));

        tableColumnUCName.setCellValueFactory(new PropertyValueFactory<>("nume"));

        tableColumnChat.setCellValueFactory(new PropertyValueFactory<>("continut"));

        tableColumnGroups.setCellValueFactory(new PropertyValueFactory<>("nume"));

        tableColumn1pdf1_1.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumn2pdf1_1.setCellValueFactory(new PropertyValueFactory<>("data"));

        tableColumn1pdf1_2.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumn2pdf1_2.setCellValueFactory(new PropertyValueFactory<>("data"));

        tableColumn1pdf2.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumn2pdf2.setCellValueFactory(new PropertyValueFactory<>("data"));

        tableColumnName1.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnPrenume1.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnEName.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumnEStart.setCellValueFactory(new PropertyValueFactory<>("data_start"));
        tableColumnEEnd.setCellValueFactory(new PropertyValueFactory<>("data_end"));

        tableColumnUEName.setCellValueFactory(new PropertyValueFactory<>("nume"));
        tableColumnUEStart.setCellValueFactory(new PropertyValueFactory<>("data_start"));
        tableColumnUEEnd.setCellValueFactory(new PropertyValueFactory<>("data_end"));

        serchField.textProperty().addListener((x) -> handleserchField());
        serchField1.textProperty().addListener((x) -> handleserchField1());

        tableUsers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }


    private void handleserchField1() {
        modelGrade1.setAll(getListaFriend()
                .stream()
                .filter(x -> {
                    String nume_user = serchField1.getText();
                    return x.getFirstName().startsWith(nume_user);
                })
                .collect(Collectors.toList())
        );
    }

    @FXML
    private void handleUserSelectat() {
        tableGroups.getSelectionModel().clearSelection();
        UserChat userChat = tableUsersChat.getSelectionModel().getSelectedItem();
        long size =service.size_conversation_users(user.getId(),userChat.getId());
        MAXChat = size/2;
        if(size%2!=0)
            MAXChat++;
        countChat = MAXChat;
        if (userChat != null) {
            initTableChat(userChat.getId(),countChat);
        }
    }

    //aici modifici
    public void setHomeUserService(BDService service, Utilizator user, PageUser page_User) {
        this.service = service;
        this.user = user;
        this.page_User = page_User;

        idLabel.setText(page_User.getId().toString());
        FirstNameLabel.setText(page_User.getNume());
        LastNameLabel.setText(page_User.getPrenume());

        count1 = 1L;
        Count1Label.setText(count1.toString());
        Long size = service.number_user();
        MAX1 = size / 6;
        if (size % 6 != 0)
            MAX1++;

        count2 = 1L;
        Count2Label.setText(count2.toString());
        Long size2 = service.size_event();
        MAX2 = size2 / 6;
        if (size2 % 6 != 0)
            MAX2++;

        countME = 1L;
        CountMELabel.setText(countME.toString());
        Long sizeME = service.size_my_events(user.getId());
        MAXME = sizeME / 6;
        if (sizeME % 6 != 0)
            MAXME++;
        service.addObserver(this);
        page_User.addObserver(this);
        //aici setezi toti userii in lista observabila
        //modelGrade.setAll((Collection<? extends Utilizator>) service.getAll());
        //modelGrade.setAll(service.get_all_users(count1));
        modelGrade1.setAll(getListaFriend());
        modelEvent.setAll(service.get_all_events(count2));
        modelUserEvent.setAll(page_User.getList_event());
        modelMyEvents.setAll(service.get_all_my_events(user.getId()));
        ///modelUserEvent.setAll(service.get_all_user_event())
        // modelFriend.setAll(service.ge)
        initTableFriends();
        initTableFriendRequest();
        initTableUsersChat();
        initTableUsers();
        initTableGroups();
        verificaEvent();
        // initTableChat((long) 19);


    }

    public void verificaEvent(){
        thread.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                List<String> events = service.get_all_my_events(user.getId()).stream()
                        .filter(x->{
                            LocalDate data_start = x.getData_start().toLocalDate();
                            return data_start.equals(LocalDate.now()) && x.getNotify() == 1;
                        })
                        .filter(x->{
                            Time time = x.getTime_start();
                            return (time.toLocalTime().getHour() - LocalTime.now().getHour() <= 1) && (time.toLocalTime().getHour() - LocalTime.now().getHour() >=0);
                        })
                        .map(x->{
                            String mesaj = "Evenimentul "+x.getNume()+ " incepe in mai putin de o ora!\n";
                            return mesaj;
                        })
                        .collect(Collectors.toList());
//                List<String> events = new ArrayList<>();
//                for(Event x : service.get_all_my_events(user.getId()))
//                {
//                    LocalDate data_start =x.getData_start().toLocalDate();
//                    if(data_start.equals(LocalDate.now()) && x.getNotify() == 1){
//                        Time time = x.getTime_start();
//                        System.out.println(time.toLocalTime().getHour());
//                        int ele1 = time.toLocalTime().getHour();
//                        int ele = LocalTime.now().getHour();
//                        int ele2 = time.toLocalTime().getMinute();
//                        int ele3 = LocalTime.now().getMinute();
//                        System.out.println(ele);
//                        System.out.println(time.toLocalTime().getHour());
//                        System.out.println(LocalTime.now().getHour());
//                        if ((time.toLocalTime().getHour() - LocalTime.now().getHour() <= 1) && (time.toLocalTime().getHour() - LocalTime.now().getHour() >=0)){
//                            String mesaj = "Evenimentul "+x.getNume()+ " incepe in mai putin de o ora!\n";
//                            events.add(mesaj);
//                        }
//                    }
//                }
                if(!events.isEmpty()) {
                    StringBuilder msg = new StringBuilder();
                    for(String ele:events) {
                        msg.append(ele);
                    }
                    Platform.runLater(()->{
                        MessageAlert.showErrorMessage(null, msg.toString());
                    });
                }
            }
        }, 10, 20, TimeUnit.SECONDS);
    }


    public void handleserchField() {
        initTableUsers();

    }


    public void handleLogOutButton(ActionEvent event) throws IOException {
        thread.shutdown();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/homePage.fxml"));
        Parent createaccountParent = loader.load();

        HomePageController controller = loader.getController();
        controller.setService(service);

        Scene createaccountScene = new Scene(createaccountParent, 700, 500);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(createaccountScene);
        window.show();
    }

    public void handleAddFriend(ActionEvent actionEvent) {
        Utilizator userselect = tableUsers.getSelectionModel().getSelectedItem();
        if (userselect == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un utilizator!");
        } else if (userselect.getId().equals(user.getId())) {
            MessageAlert.showErrorMessage(null, "Uyilizatorul selectat esti tu!");
        } else {
            try {
                service.add_service_friendrequest(user.getId(), userselect.getId());
                page_User.delete_FR(service.get_all_friendrequest(user.getId()));
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Cerere trimisa!");
                SendFriendRequest.setVisible(false);
                DeleteFriendRequest.setVisible(true);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    public void handleCreateGroup(ActionEvent actionEvent) throws IOException {
//        List<Utilizator> list = tableUsers.getSelectionModel().getSelectedItems();
//        String nume = numeGrupField.getText();
//        if (list.size() <= 1) {
//            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi 2 sau mai multi utilizatori!");
//        } else if (nume.equals("")) {
//            MessageAlert.showErrorMessage(null, "Trebuie sa introduci un nume grupului");
//
//        } else {
//            try {
//                List<Long> list_user = new ArrayList<>();
//                for (Utilizator userr : list) {
//                    if (userr.getId().equals(user.getId()))
//                        throw new ValidationException("Nu te poti selecta si pe tine!");
//                    list_user.add(userr.getId());
//                }
//                list_user.add(user.getId());
//                service.create_group(nume, list_user);
//                numeGrupField.setText("");
//            } catch (ValidationException e) {
//                MessageAlert.showErrorMessage(null, e.getMessage());
//            }
//        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/createGroup.fxml"));
        Parent root = loader.load();

        CreateGroupController controller = loader.getController();
        controller.setController(service, user);
        Stage stage = new Stage();
        Scene createaccountScene = new Scene(root, 600, 400);
        //Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        stage.setScene(createaccountScene);
        stage.show();
    }

    @Override
    public void update(TaskChangeEvent taskChangeEvent) {
        if (taskChangeEvent.getType() == ChangeEventType.FRIEND_REQUEST) {
            initTableFriendRequest();
        }
        if (taskChangeEvent.getType() == ChangeEventType.DELETE_FRIEND_REQUEST) {
            initTableFriendRequest();
        }
        if (taskChangeEvent.getType() == ChangeEventType.DELETE_FRIEND) {
            initTableFriends();
            modelGrade1.setAll(getListaFriend());
        }
        if (taskChangeEvent.getType() == ChangeEventType.ADD_FRIEND) {
            initTableFriends();
            initTableFriendRequest();
            modelGrade1.setAll(getListaFriend());
        }
        if (taskChangeEvent.getType() == ChangeEventType.SEND_PRIVATE_MESSAGE) {
            initTableUsersChat();

        }
        if (taskChangeEvent.getType() == ChangeEventType.CREATE_GROUP) {
            initTableGroups();
        }
        if (taskChangeEvent.getType() == ChangeEventType.ADD_EVENT) {
            Long size2 = service.size_event();
            MAX2 = size2 / 6;
            if (size2 % 6 != 0)
                MAX2++;
            modelEvent.setAll(service.get_all_events(count2));
        }
        if (taskChangeEvent.getType() == ChangeEventType.JOIN_EVENT) {
            Long sizeME = service.size_my_events(user.getId());
            MAXME = sizeME / 6;
            if (sizeME % 6 != 0)
                MAXME++;
            modelUserEvent.setAll(service.get_all_user_event(user.getId(), countME));
            modelMyEvents.setAll(service.get_all_my_events(user.getId()));
        }

        if (taskChangeEvent.getType() == ChangeEventType.LEAVE_EVENT) {
            Long sizeME = service.size_my_events(user.getId());
            MAXME = sizeME / 6;
            if (sizeME % 6 != 0)
                MAXME++;
            modelUserEvent.setAll(service.get_all_user_event(user.getId(), countME));
            modelMyEvents.setAll(service.get_all_my_events(user.getId()));
            modelEvent.setAll(service.get_all_events(count2));
        }

        if (taskChangeEvent.getType() == ChangeEventType.NOTIFY) {
            //Long sizeME = service.size_my_events(user.getId());
            //MAXME = sizeME / 2;
            //if (sizeME % 2 != 0)
            //    MAXME++;
            modelUserEvent.setAll(service.get_all_user_event(user.getId(), countME));
            modelMyEvents.setAll(service.get_all_my_events(user.getId()));
        }

    }


    public void handleDeleteFriendRequest(ActionEvent actionEvent) {
        Utilizator userselect = tableUsers.getSelectionModel().getSelectedItem();
        if (userselect == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un utilizator!");
        } else {
            try {
                service.retragere_cerere(user.getId(), userselect.getId());
                page_User.retragere_FR(service.get_all_friendrequest(user.getId()));
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Cererea a fost stearsa!");
                SendFriendRequest.setVisible(true);
                DeleteFriendRequest.setVisible(false);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    public void handleSendMessage(ActionEvent actionEvent) throws IOException {
//
//        Utilizator userselect = tableUsers.getSelectionModel().getSelectedItem();
//        String text = sendmessage1TextArea.getText();
//        if (userselect == null) {
//            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un utilizator!");
//        } else if (text.equals("")) {
//            MessageAlert.showErrorMessage(null, "Trebuie sa introduci un mesaj");
//        } else {
//            try {
//                Long iddd = service.add_message(user.getId(), text, userselect.getId(), (long) 0, null);
//                page_User.add_message(iddd, user.getId(), text, userselect.getId(), (long) 0, null);
//                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Mesaj trimis!");
//                sendmessage1TextArea.setText("");
//            } catch (ValidationException e) {
//                MessageAlert.showErrorMessage(null, e.getMessage());
//            }
//        }



        Utilizator userselect = tableUsers.getSelectionModel().getSelectedItem();
        if (userselect == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un utilizator!");
        }  else if(userselect.getId().equals(user.getId())){
            MessageAlert.showErrorMessage(null, "Utilizatorul selectat esti tu!");
        }else {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/sendMessage.fxml"));
            Parent root = loader.load();

            SendMessageController controller = loader.getController();
            controller.setController(service,user,userselect,page_User);

            Stage stage = new Stage();
            Scene createaccountScene = new Scene(root, 400, 306);
            //Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            stage.setScene(createaccountScene);
            stage.show();
        }
    }

    public void handleDeleteFriend(ActionEvent actionEvent) {
        Utilizator userselect = tableFriends.getSelectionModel().getSelectedItem();
        if (userselect == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un prieten!");
        } else {
            try {
                if (user.getId() < userselect.getId())
                    service.delete_friendship(user.getId(), userselect.getId());
                else
                    service.delete_friendship(userselect.getId(), user.getId());
                service.delete_FE(userselect.getId(), user.getId());
                page_User.delete_friend(service.get_all_friends(user.getId()));
                page_User.delete_FR(service.get_all_friendrequest(user.getId()));
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Prieten sters!");
            } catch (ValidationException | IOException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    public void handleAcceptFriendRequest(ActionEvent actionEvent) {
        FriendRGUI users = tableFriendsRequest.getSelectionModel().getSelectedItem();
        if (users == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi o cerere!");
        } else if (!users.getStatus().equals("PENDING")) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi o cerere de prietenie!");
        } else {
            try {
                service.service_accept_friendrequest(user.getId(), users.getId());
                page_User.accept_friend_request(service.get_all_friends(user.getId()), service.get_all_friendrequest(user.getId()));
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Prieten adaugat!");
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }

    }

    public void handleDeclineFriendRequest(ActionEvent actionEvent) {
        FriendRGUI users = tableFriendsRequest.getSelectionModel().getSelectedItem();
        if (users == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi p cerere!");
        } else if (!users.getStatus().equals("PENDING")) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi o cerere de prietenie!");
        } else {
            try {
                service.service_delete_friendrequest(user.getId(), users.getId());
                page_User.decline_friend_request(service.get_all_friendrequest(user.getId()));
                MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Cerere stearsa!");
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }


    public void handleSend(MouseEvent mouseEvent) {
        UserChat userChat = tableUsersChat.getSelectionModel().getSelectedItem();
        int userIndex = tableUsersChat.getSelectionModel().getSelectedIndex();
        Group grup = tableGroups.getSelectionModel().getSelectedItem();
        MessageChat msg = tableChat.getSelectionModel().getSelectedItem();
        String text = messageTextArea.getText();
        if (text.equals("")) {
            MessageAlert.showErrorMessage(null, "Trebuie sa introduci un mesaj!");
        } else if (msg == null && userChat != null) {
            try {
                Long iddd = service.add_message(user.getId(), text, userChat.getId(), (long) 0, null);
                page_User.add_message(iddd, user.getId(), text, userChat.getId(), (long) 0, null);
                messageTextArea.setText("");
                long size = service.size_conversation_users(user.getId(), userChat.getId());
                MAXChat = size/2;
                if(size%2!=0)
                    MAXChat++;
                countChat =MAXChat;
                initTableChat(userChat.getId(),countChat);
                tableUsersChat.getSelectionModel().select(userIndex);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        } else if (msg == null && grup != null) {

            try {
                Long idd = service.add_message(user.getId(), text, null, (long) 0, grup.getId());
                page_User.add_message_grup(idd, user.getId(), text, null, (long) 0, grup.getId());
                messageTextArea.setText("");
                long size = service.size_conversation_group(grup.getId());
                MAXChat = size/2;
                if(size%2!=0)
                    MAXChat++;
                countChat =MAXChat;
                initTableChat2(grup.getId(),countChat);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        } else if (msg != null && userChat != null) {
            try {
                if (msg.getFrom().equals(user.getId())) {
                    Long iddd = service.add_message(user.getId(), text, userChat.getId(), msg.getId_message(), null);
                    page_User.add_message(iddd, user.getId(), text, userChat.getId(), msg.getId_message(), null);
                } else {
                    Long iddd = service.add_message(user.getId(), text, msg.getFrom(), msg.getId_message(), null);
                    page_User.add_message(iddd, user.getId(), text, msg.getFrom(), msg.getId_message(), null);
                }
                messageTextArea.setText("");

                long size = service.size_conversation_users(user.getId(), userChat.getId());
                MAXChat = size/2;
                if(size%2!=0)
                    MAXChat++;
                countChat =MAXChat;
                initTableChat(userChat.getId(),countChat);
                tableUsersChat.getSelectionModel().select(userIndex);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }

        } else if (msg != null && grup != null) {
            try {
                Long idd = service.add_message(user.getId(), text, null, msg.getId_message(), grup.getId());
                page_User.add_message_grup(idd, user.getId(), text, null, msg.getId_message(), grup.getId());
                messageTextArea.setText("");
                long size = service.size_conversation_group(grup.getId());
                MAXChat = size/2;
                if(size%2!=0)
                    MAXChat++;
                countChat =MAXChat;
                initTableChat2(grup.getId(),countChat);
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    public void handleSendPrivate(MouseEvent mouseEvent) {
        Group grup = tableGroups.getSelectionModel().getSelectedItem();
        MessageChat msg = tableChat.getSelectionModel().getSelectedItem();
        String text = messageTextArea.getText();
        if (msg == null || grup == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa alegi un grup,un mesaj si un text introdus pentru a putea apasa pe buton!");
        } else if (msg.getFrom().equals(user.getId())) {
            MessageAlert.showErrorMessage(null, "nu iti poti trimite mesaj in privat tie");

        } else if (grup != null && msg != null && !text.equals("")) {
            try {
                Long idd = service.add_message(user.getId(), text, msg.getFrom(), msg.getId_message(), null);
                page_User.add_message(idd, user.getId(), text, msg.getFrom(), msg.getId_message(), null);
                messageTextArea.setText("");
            } catch (ValidationException e) {
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        } else {
            MessageAlert.showErrorMessage(null, "Trebuie sa alegi un grup,un mesaj si un text introdus pentru a putea apasa pe buton!");
        }

    }

    public void handleGroupsSelectat(MouseEvent mouseEvent) {
        tableUsersChat.getSelectionModel().clearSelection();
        Group grup = tableGroups.getSelectionModel().getSelectedItem();
        long size =service.size_conversation_group(grup.getId());
        MAXChat = size/2;
        if(size%2!=0)
            MAXChat++;
        countChat = MAXChat;
        if (grup != null && MAXChat!=0) {
            initTableChat2(grup.getId(),countChat);
        }
    }

    public List<PDF1> getAllFriend(LocalDate start, LocalDate end) {
        return StreamSupport.stream(service.getAll2().spliterator(), false)
                .filter(x -> x.getId().getRight().equals(user.getId()) || x.getId().getLeft().equals(user.getId()))
                .filter(x -> {
                    String yy = x.getDate().toString();
                    String xx = yy.substring(0, Math.min(yy.length(), 10));
                    return xx.compareTo(start.toString()) >= 0 && xx.compareTo(end.toString()) < 0;
                })
                .map(x -> {
                    if (x.getId().getRight().equals(user.getId())) {
                        Utilizator userr = service.find_user(x.getId().getLeft());
                        String str = userr.getFirstName() + " " + userr.getLastName();
                        return new PDF1(str, x.getDate().toString());
                    } else {
                        Utilizator userr = service.find_user(x.getId().getRight());
                        String str = userr.getFirstName() + " " + userr.getLastName();
                        return new PDF1(str, x.getDate().toString());
                    }

                })
                .collect(Collectors.toList());
    }


    public void handleGenereazaPDF1Button(MouseEvent mouseEvent) {
        LocalDate start = data_pdf1_start.getValue();
        LocalDate end = data_pdf1_end.getValue();
        try {
            if (start == null || end == null)
                throw new ValidationException("Trebuie sa selectezi o perioada calendaristica!");
            if (start.toString().compareTo(end.toString()) >= 0)
                throw new ValidationException("Datele trebuie sa fie selectate in ordine cronologica!");
            String numepdf = user.getFirstName() + "_1.pdf";
            modelpdf.setAll(getAllFriend(start, end));
            modelpdf2.setAll(service.get_all_message_for(user.getId(), start, end));
            PDF pdf = new PDF();
            pdf.generare_pdf(numepdf, modelpdf, modelpdf2);

        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());

        }


    }

    public void handleGenereazaPDF2Button(MouseEvent mouseEvent) {
        Utilizator userr = tableUsers1.getSelectionModel().getSelectedItem();
        LocalDate start = data_pdf2_start.getValue();
        LocalDate end = data_pdf2_end.getValue();
        try {
            if (start == null || end == null)
                throw new ValidationException("Trebuie sa selectezi o perioada calendaristica!");
            if (start.toString().compareTo(end.toString()) >= 0)
                throw new ValidationException("Datele trebuie sa fie selectate in ordine cronologica!");
            if (userr == null)
                throw new ValidationException("Trebuie sa selectezi un user");
            if (userr.getId().equals(user.getId()))
                throw new ValidationException("Nu te poti selecta pe tine!");
            String numepdf = user.getFirstName() + "_2.pdf";
            modelpdf3.setAll(service.get_all_message_from_one_user(userr.getId(), user.getId(), start, end));
            PDF pdf = new PDF();
            pdf.generare_pdf2(numepdf, modelpdf3);


        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }

    }


    public List<Utilizator> getListaUseri() {
        return (List<Utilizator>) service.getAll();
    }

    public void initTableUsers() {
        String nume = serchField.getText();

        modelGrade.setAll(service.get_all_users(count1,nume));
    }

    public List<Utilizator> getListaFriend() {
//        return service.get_all_friends(user.getId())
//                .stream()
//                .map(x->service.find_user(x))
//                .collect(Collectors.toList());
        return page_User.getList_friends()
                .stream()
                .map(x -> service.find_user(x))
                .collect(Collectors.toList());
    }

    public void initTableFriends() {
        modelFriend.setAll(getListaFriend());
    }

    public List<FriendRGUI> getListaFR() {
//        return service.get_all_friendrequest(user.getId())
//                .stream()
//                .map(x->{
//                    Utilizator u = service.find_user(x.getId().getLeft());
//                    return new FriendRGUI(u.getFirstName(),u.getLastName(),x.getStatus(),x.getData_time(),x.getId().getLeft());
//
//                })
//                .collect(Collectors.toList());
        return page_User.getList_friendsRequrest()
                .stream()
                .map(x -> {
                    Utilizator u = service.find_user(x.getId().getLeft());
                    return new FriendRGUI(u.getFirstName(), u.getLastName(), x.getStatus(), x.getData_time(), x.getId().getLeft());

                })
                .collect(Collectors.toList());
    }

    public void initTableFriendRequest() {
        modelFriendRequest.setAll(getListaFR());
    }

    public List<UserChat> getListaUserChat() {
        return service.get_users_id_chat(user.getId())
                .stream()
                .map(x -> {
                    Utilizator u = service.find_user(x);
                    return new UserChat(u.getFirstName(), x);
                })
                .collect(Collectors.toList());
    }

    public void initTableUsersChat() {
        modelUserChat.setAll(getListaUserChat());
    }

    public List<MessageChat> getListaMesaje(Long id,Long count) {
        return service.get_conversation(user.getId(), id,count)
                .stream()
                .map(x->{
                    String nume = service.find_user(x.getFrom()).getFirstName();
                    String str;
                    if(x.getReply_message() == 0) {
                        str = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData();
                    }
                    else
                    {
                        str  = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData() +"\n Reply: " + service.get_message(x.getReply_message()).getMessage();
                    }
                    return new MessageChat(x.getId_msg(),x.getFrom(),str);
                })
                .collect(Collectors.toList());
//        return page_User.getConversation(id)
//                .stream()
//                .map(x -> {
//                    String nume = service.find_user(x.getFrom()).getFirstName();
//                    String str;
//                    if (x.getReply_message() == 0) {
//                        str = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData();
//                    } else {
//                        str = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData() + "\n Reply: " + service.get_message(x.getReply_message()).getMessage();
//                    }
//                    return new MessageChat(x.getId_msg(), x.getFrom(), str);
//                })
//                .collect(Collectors.toList());
    }


    public void initTableChat(Long id,Long count) {
        modelChat.setAll(getListaMesaje(id,count));
    }

    public void initTableGroups() {
        modelGroup.setAll(service.get_All_groups(user.getId()));
    }

    private void initTableChat2(Long id,Long count) {
        modelChat.setAll(getListaMesajeGrup(id,count));
    }

    private List<MessageChat> getListaMesajeGrup(Long id,Long count) {

        return service.get_chat_grup(id,count)
                .stream()
                .map(x -> {
                    String nume = service.find_user(x.getFrom()).getFirstName();
                    String str;
                    if (x.getReply_message() == 0) {
                        str = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData();
                    } else {
                        str = "From: " + nume + "\n Message: " + x.getMessage() + "\n Data: " + x.getData() + "\n Reply: " + service.get_message(x.getReply_message()).getMessage();
                    }
                    return new MessageChat(x.getId_msg(), x.getFrom(), str);
                })
                .collect(Collectors.toList());

    }

    public void handleChangeData(MouseEvent mouseEvent) {
        LocalDate start = data_pdf1_start.getValue();
        LocalDate end = data_pdf1_end.getValue();
        try {
            if (start == null || end == null)
                throw new ValidationException("Trebuie sa selectezi o perioada calendaristica!");
            if (start.toString().compareTo(end.toString()) >= 0)
                throw new ValidationException("Datele trebuie sa fie selectate in ordine cronologica!");
            modelpdf.setAll(getAllFriend(start, end));
            modelpdf2.setAll(service.get_all_message_for(user.getId(), start, end));

        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());

        }
    }

    public void handleChangeData2(MouseEvent mouseEvent) {
        Utilizator userr = tableUsers1.getSelectionModel().getSelectedItem();
        LocalDate start = data_pdf2_start.getValue();
        LocalDate end = data_pdf2_end.getValue();
        try {
            if (start == null || end == null)
                throw new ValidationException("Trebuie sa selectezi o perioada calendaristica!");
            if (start.toString().compareTo(end.toString()) >= 0)
                throw new ValidationException("Datele trebuie sa fie selectate in ordine cronologica!");
            if (userr == null)
                throw new ValidationException("Trebuie sa selectezi un user");
            if (userr.getId().equals(user.getId()))
                throw new ValidationException("Nu te poti selecta pe tine!");
            modelpdf3.setAll(service.get_all_message_from_one_user(userr.getId(), user.getId(), start, end));

        } catch (ValidationException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }

    public void handleCreateEvent(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/createEventPage.fxml"));
        Parent root = loader.load();

        CreateEventController controller = loader.getController();
        controller.setController(service);

        Stage stage = new Stage();
        Scene createaccountScene = new Scene(root, 400, 400);
        //Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        stage.setScene(createaccountScene);
        stage.show();

    }

    public void handleDetailsButton(ActionEvent actionEvent) throws IOException {
        Event event = tableEvent.getSelectionModel().getSelectedItem();
        Event eventuser = tableUserEvent.getSelectionModel().getSelectedItem();

        if (event == null && eventuser == null) {
            MessageAlert.showErrorMessage(null, "Trebuie sa selectezi un event!");
        } else {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/detailsEventPage.fxml"));
            Parent root = loader.load();

            DetailsEventController controller = loader.getController();
            if (event == null) {
                controller.setController(eventuser, service, page_User);
            } else {
                controller.setController(event, service, page_User);
            }
            Stage stage = new Stage();
            Scene createaccountScene = new Scene(root, 400, 400);
            //Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            stage.setScene(createaccountScene);
            stage.show();
        }
    }

    public void handleEvent(MouseEvent mouseEvent) {
        tableUserEvent.getSelectionModel().clearSelection();

    }

    public void handleUserEvent(MouseEvent mouseEvent) {
        tableEvent.getSelectionModel().clearSelection();
    }


    public void handleRight1Button(MouseEvent mouseEvent) {
        if (count1 + 1 <= MAX1) {
            count1++;
            Count1Label.setText(count1.toString());
            initTableUsers();
        }
    }

    public void handleLeft1Button(MouseEvent mouseEvent) {
        if (count1 - 1 >= 1) {
            count1--;
            Count1Label.setText(count1.toString());
            initTableUsers();
        }
    }

    public void handleRight2Button(MouseEvent mouseEvent) {
        if (count2 + 1 <= MAX2) {
            count2++;
            Count2Label.setText(count2.toString());
            modelEvent.setAll(service.get_all_events(count2));

        }
    }

    public void handleLeft2Button(MouseEvent mouseEvent) {
        if (count2 - 1 >= 1) {
            count2--;
            Count2Label.setText(count2.toString());
            modelEvent.setAll(service.get_all_events(count2));

        }
    }

    public void handleRightMEButton(MouseEvent mouseEvent) {
        if (countME + 1 <= MAXME) {
            countME++;
            CountMELabel.setText(countME.toString());
            modelUserEvent.setAll(service.get_all_user_event(user.getId(), countME));

        }
    }

    public void handleLeftMEButton(MouseEvent mouseEvent) {
        if (countME - 1 >= 1) {
            countME--;
            CountMELabel.setText(countME.toString());
           modelUserEvent.setAll(service.get_all_user_event(user.getId(),countME));

        }
    }



    /*................................................*/
    public void createGroupHoverHandle(MouseEvent mouseEvent) {
        createGroupImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_people_24px_1.png"))));
    }

    public void createGroupExitedHandle(MouseEvent mouseEvent) {
        createGroupImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_people_24px.png"))));
    }

    public void handleRight1Entered(MouseEvent mouseEvent) {
        Right1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px.png"))));

    }

    public void handleRight1Exited(MouseEvent mouseEvent) {
        Right1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px_3.png"))));
    }

    public void handleLeft1Entered(MouseEvent mouseEvent) {
        Left1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px.png"))));
    }

    public void handleRightExited(MouseEvent mouseEvent) {
        Left1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px_2.png"))));
    }

    public void sendMessageEnteredHandle(MouseEvent mouseEvent) {
        sendMessage1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_paper_plane_32px.png"))));
    }

    public void sendMessageExitedHandle(MouseEvent mouseEvent) {
        sendMessage1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_paper_plane_32px_1.png"))));
    }

    public void addFriendEnteredHandle(MouseEvent mouseEvent) {
        addFriendImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_add_user_group_man_man_50px_1.png"))));
    }

    public void addFriendExitedHandle(MouseEvent mouseEvent) {
        addFriendImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_add_user_group_man_man_50px.png"))));

    }

    public void deleteFDEnteredHandle(MouseEvent mouseEvent) {
        deleteFRImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_delete_26px.png"))));
    }

    public void deleteFRExitedHandle(MouseEvent mouseEvent) {
        deleteFRImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_delete_26px_2.png"))));
    }

    public void handleRight2Entered(MouseEvent mouseEvent) {
        Right2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px.png"))));

    }

    public void handleRight2Exited(MouseEvent mouseEvent) {
        Right2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px_3.png"))));
    }

    public void handleLeft2Entered(MouseEvent mouseEvent) {
        Left2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px.png"))));

    }

    public void handleLeft2Exited(MouseEvent mouseEvent) {
        Left2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px_2.png"))));
    }

    public void handleRightMEEntered(MouseEvent mouseEvent) {
        RightMEImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px.png"))));

    }

    public void handleRightMEExited(MouseEvent mouseEvent) {
        RightMEImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_right_26px_3.png"))));
    }

    public void handleLeftMEEntered(MouseEvent mouseEvent) {
        LeftMEImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px.png"))));

    }

    public void handleLeftMEExited(MouseEvent mouseEvent) {
        LeftMEImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_left_26px_2.png"))));
    }


    @FXML
    private ImageView HomeImage;
    public void handleSelectHome(javafx.event.Event event) {
        HomeImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_user_30px_1.png"))));

    }

    public void handleDeselectHome(javafx.event.Event event) {
        HomeImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/home.png"))));

    }

    @FXML
    private ImageView UsersImage;
    public void handleUsersEntered(MouseEvent mouseEvent) {
       UsersImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_user_groups_24px.png"))));

    }

    public void handleUsersExited(MouseEvent mouseEvent) {
        UsersImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_user_groups_26px.png"))));
    }

    @FXML
    private ImageView FriendsImage;

    public void handleFriendsEntered(MouseEvent mouseEvent) {
        FriendsImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_add_user_group_man_man_32px_1.png"))));
    }

    public void handleFriendsExited(MouseEvent mouseEvent) {
        FriendsImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/add_user.png"))));
    }

    @FXML
    private ImageView ChatImage;
    public void handleChatEntered(MouseEvent mouseEvent) {
        ChatImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_chat_bubble_30px.png"))));

    }

    public void handleChatExited(MouseEvent mouseEvent) {
        ChatImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/ch.png"))));

    }

    @FXML
    private ImageView PDF1Image;
    public void handlePDF1Entered(MouseEvent mouseEvent) {
        PDF1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_64px.png"))));

    }

    public void handlePDF1Exited(MouseEvent mouseEvent) {
        PDF1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/pdf.png"))));

    }

    @FXML
    private ImageView PDF2Image;
    public void handlePDF2Entered(MouseEvent mouseEvent) {
        PDF2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_64px.png"))));

    }

    public void handlePDF2Exited(MouseEvent mouseEvent) {
        PDF2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/pdf.png"))));

    }

    @FXML
    private ImageView EventImage;
    public void handleEventEn(MouseEvent mouseEvent) {
        EventImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_schedule_26px.png"))));

    }

    public void handleEventEx(MouseEvent mouseEvent) {
        EventImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/ev.png"))));

    }

    @FXML
    private ImageView OK1Image;

    public void handleOK1Entered(MouseEvent mouseEvent) {
        OK1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_ok_24px_1.png"))));
    }

    public void handleOK1Exited(MouseEvent mouseEvent) {
        OK1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_ok_24px.png"))));
    }

    @FXML
    private  ImageView PDFF1Image;
    public void handlePDFF1Entered(MouseEvent mouseEvent) {
        PDFF1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_26px_1.png"))));
    }

    public void handlePDFF1Exited(MouseEvent mouseEvent) {
        PDFF1Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_26px.png"))));
    }

    @FXML
    private ImageView OK2Image;

    public void handleOK2Entered(MouseEvent mouseEvent) {
        OK2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_ok_24px_1.png"))));
    }

    public void handleOK2Exited(MouseEvent mouseEvent) {
        OK2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_ok_24px.png"))));
    }

    @FXML
    private  ImageView PDFF2Image;
    public void handlePDFF2Entered(MouseEvent mouseEvent) {
        PDFF2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_26px_1.png"))));
    }

    public void handlePDFF2Exited(MouseEvent mouseEvent) {
        PDFF2Image.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_pdf_26px.png"))));
    }


    @FXML
    private ImageView ReplyImage;
    public void handleReplyEntered(MouseEvent mouseEvent) {
        ReplyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_forward_message_24px_1.png"))));
    }

    public void handleReplyExited(MouseEvent mouseEvent) {
        ReplyImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_forward_message_24px_3.png"))));

    }

    @FXML
    private ImageView SendChatImage;
    public void handleSendChatEntered(MouseEvent mouseEvent) {
        SendChatImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_paper_plane_26px_1.png"))));

    }

    public void handleSendChatExited(MouseEvent mouseEvent) {
        SendChatImage.setImage(new Image(String.valueOf(getClass().getResource("/design/Poze/icons8_paper_plane_26px_3.png"))));
    }

    @FXML
    private AnchorPane ancorePane1;
    @FXML
    private AnchorPane ancorePane2;


    @FXML
    private Button SendFriendRequest;

    @FXML
    private Button DeleteFriendRequest;


    public void handleDeselectTableUsers(MouseEvent mouseEvent) {
        tableUsers.getSelectionModel().clearSelection();
        SendFriendRequest.setVisible(false);
        DeleteFriendRequest.setVisible(false);
    }


    public void handleTableUserSelectat(MouseEvent mouseEvent) {
        Utilizator userr = tableUsers.getSelectionModel().getSelectedItem();
        if(userr.getId() != user.getId()) {
            if (service.exista_cerere_pending_from_to(user.getId(), userr.getId())) {
                DeleteFriendRequest.setVisible(true);
                SendFriendRequest.setVisible(false);
            } else {
                if (!service.sunt_prieteni(user.getId(), userr.getId())) {
                    SendFriendRequest.setVisible(true);
                    DeleteFriendRequest.setVisible(false);
                }
            }
        }
        else
        {
            SendFriendRequest.setVisible(false);
            DeleteFriendRequest.setVisible(false);
        }
    }

    public void messageScrollHandle(ScrollEvent scrollEvent) {
            System.out.println("print");
            System.out.println(MAXChat);
            UserChat userChat = tableUsersChat.getSelectionModel().getSelectedItem();
            Group grup = tableGroups.getSelectionModel().getSelectedItem();
            if(scrollEvent.getDeltaY()>0) {
                if(countChat - 1 >= 1){
                    countChat--;
                    if(userChat != null) {

                        initTableChat(userChat.getId(), countChat);
                    }
                    else {
                        initTableChat2(grup.getId(), countChat);
                    }
                }
            }
            else{
                if(countChat + 1 <= MAXChat ) {
                    countChat++;
                    if(userChat != null) {
                        initTableChat(userChat.getId(), countChat);
                    }
                    else {
                        initTableChat2(grup.getId(), countChat);
                    }
                }
            }

    }
}
