package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import socialnetwork.domain.Event;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class CreateEventController {
    BDService service;

    @FXML
    private TextField titleField;

    @FXML
    private TextArea descriptionField;

    @FXML
    private DatePicker data_start;

    @FXML
    private ComboBox<String> combo_time_start;

    @FXML
    private DatePicker data_end;

    @FXML
    private ComboBox<String> combo_time_end;

    public void setController(BDService service) {
        this.service = service;
        combo_time_start.getItems().addAll(
               "06:00",
                "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00",
                "11:30", "12:00", "12:30", "13:00", "13:30", "14:00",
                "14:30", "15:00", "15:30", "16:00", "16:30", "17:00",
                "17:30", "18:00", "18:30", "19:00", "19:30", "20:00"
        );
        combo_time_end.getItems().addAll( "06:00",
                "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00",
                "11:30", "12:00", "12:30", "13:00", "13:30", "14:00",
                "14:30", "15:00", "15:30", "16:00", "16:30", "17:00",
                "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
    }


    public void handleCreateEvent(ActionEvent actionEvent) {
        String title = titleField.getText();
        String desc = descriptionField.getText();
        LocalDate start = data_start.getValue();
        LocalDate end =data_end.getValue();
        String time_start = combo_time_start.getValue();
        String time_end=combo_time_end.getValue();

        try{
            if(start == null ||  end == null)
                throw new ValidationException("Trebuie sa selectezi o perioada calendaristica!");
            if(start.toString().compareTo(end.toString())>0)
                throw new ValidationException("Datele trebuie sa fie selectate in ordine cronologica!");
            if(start.toString().compareTo(end.toString())>0 && time_start.compareTo(time_end)>=0)
                throw new ValidationException("Ora de inceput trebuie sa fie mai mica decat ora de final!");
            if(title == null || desc == null)
                throw new ValidationException("Trebuie sa introduci un titlu si o descriere");
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            java.util.Date d1 =(java.util.Date)format.parse(time_start);
            java.sql.Time ppstime = new java.sql.Time(d1.getTime());
            SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
            java.util.Date d12 =(java.util.Date)format2.parse(time_end);
            java.sql.Time ppstime2 = new java.sql.Time(d12.getTime());



            Event ev =new Event(title,desc,start.atStartOfDay(),end.atStartOfDay(),ppstime,ppstime2);
            service.save_event(ev);
            MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Succes!", "Event Creat!");

        }catch (ValidationException | ParseException e) {
            MessageAlert.showErrorMessage(null, e.getMessage());

        }
    }
}
