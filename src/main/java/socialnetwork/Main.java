package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.FriendRequestValidator;
import socialnetwork.domain.validators.FriendsValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriedshipsDbRepository;
import socialnetwork.repository.database.FriendRequestBDRepository;
import socialnetwork.repository.database.MessageBDRepository;
import socialnetwork.repository.database.UtilizatorDbRepository;
import socialnetwork.repository.file.FriendsFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.BDService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.Ui;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
//        String fileName=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
//        String fileName2=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.prietenie");
//
//        Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileName, new UtilizatorValidator());
//        Repository<Tuple<Long,Long>, Prietenie> friendsFileRepository = new FriendsFile(fileName2,new FriendsValidator());
//        UtilizatorService service = new UtilizatorService(userFileRepository,friendsFileRepository);
//        userFileRepository.findAll().forEach(x-> System.out.println(x));

//
//        System.out.println("Reading data from database");
//        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
//        final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
//        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");
//        Repository<Long,Utilizator> userFileRepository3 =
//                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
//        Repository<Tuple<Long,Long>,Prietenie> userFileRepository4 =
//                new FriedshipsDbRepository(url,username, pasword,  new FriendsValidator());
//        MessageBDRepository messageBDRepository = new MessageBDRepository(url,username,pasword,new MessageValidator());
//        FriendRequestBDRepository friendRequestBDRepository = new FriendRequestBDRepository(url,username,pasword,new FriendRequestValidator());
//
//        BDService servicebbd = new BDService(userFileRepository3,userFileRepository4,messageBDRepository,friendRequestBDRepository);
//
//
//        //MessageService servicemsg = new MessageService(messageBDRepository);
//
//        String nume = "Dana";
//        System.out.println(nume.equals("Dana"));
//        userFileRepository3.findAll().forEach(x-> System.out.println(x));
//        Ui ui=new Ui(service,servicebbd);
//        ui.run();
        MainApp.main(args);

    }
}


