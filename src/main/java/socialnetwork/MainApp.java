package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.HomePageController;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.BDService;

public class MainApp extends Application {

    final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
    final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
    final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");
    Repository<Long, Utilizator> userFileRepository3 =
            new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());
    Repository<Tuple<Long,Long>, Prietenie> userFileRepository4 =
            new FriedshipsDbRepository(url,username, pasword,  new FriendsValidator());
    MessageBDRepository messageBDRepository = new MessageBDRepository(url,username,pasword,new MessageValidator());
    FriendRequestBDRepository friendRequestBDRepository = new FriendRequestBDRepository(url,username,pasword,new FriendRequestValidator());

    EventBDRepository eventBDRepository = new EventBDRepository(url,username, pasword, new EventValidator());

    BDService servicebbd = new BDService(userFileRepository3,userFileRepository4,messageBDRepository,friendRequestBDRepository,eventBDRepository);


    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/homePage.fxml"));
        Parent root = loader.load();

        HomePageController controller = loader.getController();
        controller.setService(servicebbd);

        primaryStage.setScene(new Scene(root,700,500));
        primaryStage.setTitle("Aplicatie");
        primaryStage.show();

    }
}
