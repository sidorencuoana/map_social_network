package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EventBDRepository {
    private final String url;
    private final String username;
    private final String password;
    private final Validator<Event> validator;

    public EventBDRepository(String url, String username, String password, Validator<Event> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public List<Event> get_all_events(Long count) {
        List<Event> list = new ArrayList<>();
        Long poz = (count-1)*6;
        String sql ="SELECT *\n" +
                "FROM event order by data_start desc\n" +
                "LIMIT 6 OFFSET "+poz+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDateTime data_start = resultSet.getTimestamp("data_start").toLocalDateTime();
                LocalDateTime data_end = resultSet.getTimestamp("data_end").toLocalDateTime();
                Time time_start = resultSet.getTime("time_start");
                Time time_end = resultSet.getTime("time_end");

                Event ev = new Event(id, nume, descriere, data_start, data_end,time_start,time_end);
                list.add(ev);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Long save(Event entity) {
        validator.validate(entity);
        Long id = null;
        String sql ="insert into event(nume, descriere, data_start, data_end,time_start,time_end)\n" +
                "values ('"+entity.getNume()+"','"+entity.getDescriere()+"','"+entity.getData_start()+"','"+entity.getData_end()+"','"+entity.getTime_start()+"','"+entity.getTime_end()+"') RETURNING id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            if(resultSet.next()) {
                id = resultSet.getLong("id");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;

    }

    public List<Event> get_all_event_one_user(Long idd,Long count) {
        List<Event> list = new ArrayList<>();
        Long poz = (count-1)*6;
        String sql = "SELECT e.id,e.nume,e.descriere,e.data_start,e.data_end,e.time_start,e.time_end,\"user_Event\".notiify FROM \"user_Event\"\n" +
                "INNER JOIN event e ON e.id = \"user_Event\".id_event\n" +
                "WHERE id_user=" + idd + "LIMIT 6 OFFSET "+poz+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDateTime data_start = resultSet.getTimestamp("data_start").toLocalDateTime();
                LocalDateTime data_end = resultSet.getTimestamp("data_end").toLocalDateTime();
                Time time_start = resultSet.getTime("time_start");
                Time time_end = resultSet.getTime("time_end");
                Long notify = resultSet.getLong("notiify");

                Event ev = new Event(id, nume, descriere, data_start, data_end,time_start,time_end,notify);
                list.add(ev);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Event> get_all_event_one_user2(Long idd) {
        List<Event> list = new ArrayList<>();
        String sql = "SELECT e.id,e.nume,e.descriere,e.data_start,e.data_end,e.time_start,e.time_end,\"user_Event\".notiify FROM \"user_Event\"\n" +
                "INNER JOIN event e ON e.id = \"user_Event\".id_event\n" +
                "WHERE id_user=" + idd+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDateTime data_start = resultSet.getTimestamp("data_start").toLocalDateTime();
                LocalDateTime data_end = resultSet.getTimestamp("data_end").toLocalDateTime();
                Time time_start = resultSet.getTime("time_start");
                Time time_end = resultSet.getTime("time_end");
                Long notify = resultSet.getLong("notiify");

                Event ev = new Event(id, nume, descriere, data_start, data_end,time_start,time_end,notify);
                list.add(ev);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public void update_notify(Long id_u,Long id_e,Long value) {
        String sql ="UPDATE \"user_Event\" SET notiify ="+value+ " where id_event="+id_e+" AND id_user="+id_u;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void join_user_event(Long id_e,Long id_u) {
        String sql="INSERT INTO \"user_Event\"(id_event, id_user, notiify) VALUES ("+id_e+","+id_u+","+1+");";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void leave_user_event(Long id_e,Long id_u) {
        String sql="DELETE FROM \"user_Event\" WHERE id_event="+id_e+"AND id_user="+id_u;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Long size(){
        Long id = null;
        String sql ="SELECT count(id) FROM event";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            if(resultSet.next()) {
                id = resultSet.getLong("count");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;
    }

    public Long size_user_event(Long id_user)
    {
        Long id = null;
        String sql ="SELECT count(id_user) FROM \"user_Event\" where id_user = " +id_user;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            if(resultSet.next()) {
                id = resultSet.getLong("count");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;
    }



}
