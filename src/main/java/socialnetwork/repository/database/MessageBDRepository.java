package socialnetwork.repository.database;


import socialnetwork.domain.Group;
import socialnetwork.domain.Message;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageBDRepository{
    private final String url;
    private final String username;
    private final String password;
    private final Validator<Message> validator;

    public MessageBDRepository(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public Message find_message(Long id) {
        Message msg = null;
        String sql ="SELECT * FROM message WHERE id = " + id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            if(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

               msg = new Message(id3,from,text,to,data_time,reply);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if(msg==null)
            throw new ValidationException("id mesaj inexistent");
        return msg;
    }

    public List<Message> find_all_to(Long id) {
        List<Message> list = new ArrayList<>();
        String sql ="SELECT * FROM message WHERE to1 =" + id + "AND id_grup is NULL";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = null;
                msg = new Message(id3,from,text,to,data_time,reply);
                list.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }


    public List<Message> find_conversation(Long id1,Long id2,Long count) {
        List<Message> list = new ArrayList<>();
        Long poz = (count-1)*2;
        String sql ="SELECT * FROM message WHERE ((from1="+id1+" AND to1="+id2+") OR (from1="+id2+" AND to1="+id1+")) AND id_grup is NULL LIMIT 2 OFFSET " +poz+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = new Message(id3,from,text,to,data_time,reply);
                list.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;

    }

    public Long size_conversation_2_users(Long id1,Long id2) {
        Long id =null;
        String sql ="SELECT count(id) FROM message WHERE ((from1="+id1+" AND to1="+id2+") OR (from1="+id2+" AND to1="+id1+")) AND id_grup is NULL";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            if(resultSet.next()) {
                id = resultSet.getLong("count");
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;

    }
//
//
    public Long save_message(Message entity) {
        Long id = null;
        validator.validate(entity);
        String sql ="INSERT INTO message(from1,text,reply,data1,to1,id_grup) VALUES (" + entity.getFrom() + ",'" + entity.getMessage() + "',"+ entity.getReply_message() + ",'"+ entity.getData() +"',"+entity.getTo() +"," +entity.getId_grup()+") RETURNING id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
//          while (resultSet.next()){
//              Long id=resultSet.getLong("id");
//              for(Long  id2: entity.getTo()) {
//                  String sql2 = "INSERT INTO to1(id_mesaj,id_user) VALUES(" + id + "," + id2 + ")";
//                  PreparedStatement statement2 = connection.prepareStatement(sql2);
//                  statement2.execute();
            if(resultSet.next()) {
                id = resultSet.getLong("id");
            }
//              }
//          }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;
    }

    public List<Long> return_all_private_chats_user(Long id) {
        List<Long> list = new ArrayList<>();
        Map<Long,Long> mapp = new HashMap<Long, Long>();
        String sql ="SELECT DISTINCT to1,from1 FROM message WHERE (from1 =" + id + " OR  to1 = " + id +") AND id_grup is NULL";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long to = resultSet.getLong("to1");
                Long from = resultSet.getLong("from1");

                if(from == id)
                {
                    if(!mapp.containsKey(to)){
                        list.add(to);
                        mapp.put(to, (long) 1);
                    }

                }
                else
                {
                    if(!mapp.containsKey(from)) {
                        list.add(from);
                        mapp.put(from, (long) 1);
                    }
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public void save_group(String nume,List<Long> list) {
        String sql = "INSERT INTO groups(nume) VALUES ('" +nume + "') RETURNING id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
              while (resultSet.next()){
              Long id=resultSet.getLong("id");
              for(Long  id2: list) {
                  String sql2 = "INSERT INTO group_users(id_grup,id_user) VALUES(" + id + "," + id2 + ")";
                  PreparedStatement statement2 = connection.prepareStatement(sql2);
                  statement2.execute();
              }
          }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Group> get_all_groups(Long id) {
        List<Group> lista = new ArrayList<>();
        String sql ="SELECT G.id,G.nume FROM groups G INNER JOIN group_users gu on G.id = gu.id_grup WHERE id_user="+id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long idg = resultSet.getLong("id");
                String nume = resultSet.getString("nume");

                Group g = new Group(idg,nume);
                lista.add(g);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public List<Message> get_all_message_group(Long id,Long count) {
        List<Message> lista = new ArrayList<>();
        Long poz = (count-1)*2;
        String sql = "SELECT * FROM message WHERE id_grup="+id +" LIMIT 2 OFFSET "+poz;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = new Message(id3, from, text, to, data_time, reply);
                lista.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;

    }

    public Long size_message_group(Long id) {
        List<Message> lista = new ArrayList<>();
        Long idd =null;
        String sql = "SELECT count(id) FROM message WHERE id_grup="+id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            if(resultSet.next()) {
                idd = resultSet.getLong("count");
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return idd;

    }

    /**
     *
     * @param id_from
     * @param id_to
     * @return mesajele pe care id_to le-a primit de la id_from
     */
    public List<Message> get_all_message_from_one_user (Long id_from,Long id_to)
    {
        List<Message> lista = new ArrayList<>();
        String sql = "SELECT * FROM message WHERE id_grup is NULL AND from1="+id_from+" AND to1="+id_to;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = new Message(id3, from, text, to, data_time, reply);
                lista.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public List<Message> get_all_message_for_one_user(Long id) {
        List<Message> list = new ArrayList<>();
        String sql ="SELECT * FROM message WHERE (from1="+id+" OR to1="+id+") AND id_grup is NULL";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = new Message(id3,from,text,to,data_time,reply);
                list.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;

    }

    public  List<Message> get_all_grup_message_from_one_user(Long id) {
        List<Message> list = new ArrayList<>();
        String sql =" select m.id,m.from1,m.text,m.reply,m.data1,m.to1,m.id_grup from group_users\n" +
                "        inner join message m on group_users.id_grup = m.id_grup\n" +
                "        where id_user ="+id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
        {
            while(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                Long from = resultSet.getLong("from1");
                String text = resultSet.getString("text");
                Long reply = resultSet.getLong("reply");
                LocalDateTime data_time = resultSet.getTimestamp("data1").toLocalDateTime();
                Long to = resultSet.getLong("to1");
                Long id_grup = resultSet.getLong("id_grup");

                Message msg = new Message(id3,from,to,text,data_time,reply,id_grup);
                list.add(msg);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }


}
