package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UtilizatorDbRepository implements Repository<Long, Utilizator> {
    private String url;
    private String username;
    private String password;
    private Validator<Utilizator> validator;

    public UtilizatorDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Utilizator findOne(Long id) {
        Utilizator user = null;
        String sql ="SELECT * FROM users WHERE id = " + id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery())
             {
            if(resultSet.next()) {
                Long id3 = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");

                user = new Utilizator(firstName, lastName , password);
                user.setId((Long) id);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if(user==null)
            throw new ValidationException("id inexistent");
        return user;
    }

    @Override
    public Iterable<Utilizator> findAll() {
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users ");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");

                Utilizator utilizator = new Utilizator(firstName, lastName,password);
                utilizator.setId(id);
                users.add(utilizator);
            }
            return (Iterable<Utilizator>) users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (Iterable<Utilizator>) users;
    }

    @Override
    public List<Utilizator> findAll2(Long count,String nume) {
        List<Utilizator> list = new ArrayList<>();
        Long poz = (count-1)*6;
        String sql ="SELECT *\n" +
                "FROM users\n WHERE first_name LIKE '" +nume+
                "%' LIMIT 6 OFFSET "+poz+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");

                Utilizator utilizator = new Utilizator(firstName, lastName,password);
                utilizator.setId(id);
                list.add(utilizator);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Utilizator save(Utilizator entity) {
        validator.validate(entity);
        Utilizator user = (Utilizator) entity;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     String.format("INSERT INTO users(first_name,last_name,password) VALUES ('%s','%s','%s');",
                             user.getFirstName(), user.getLastName(), user.getPassword()))) {
            statement.execute();
            entity = null;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

    @Override
    public Utilizator delete(Long id) throws IOException {
        Utilizator user = findOne(id);
        String sql ="DELETE FROM users WHERE id=" + id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    @Override
    public Utilizator update(Utilizator entity) {
        return null;
    }

    @Override
    public void add_friends(Long id, Long id2) {

    }

    @Override
    public List<Long> all_friends(Long id) {
        return null;
    }

    @Override
    public void remove_friend(Long id, Long id2) {

    }

    @Override
    public Long size() {
        Long id = null;
        String sql ="select count(id) from users";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            if(resultSet.next()) {
                id = resultSet.getLong("count");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return id;
    }
}
