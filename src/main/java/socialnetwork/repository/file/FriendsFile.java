package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

public class FriendsFile extends AbstractFileRepository<Tuple<Long,Long>, Prietenie> {

    public FriendsFile(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }


    @Override
    public Prietenie extractEntity(List<String> attributes) {
        Prietenie elem = new Prietenie(LocalDateTime.parse(attributes.get(2)));
        Tuple<Long,Long> elem2 = new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)));
        elem.setId(elem2);
        return elem;

    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight()+";"+entity.getDate();
    }


}
