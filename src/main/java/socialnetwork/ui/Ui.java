package socialnetwork.ui;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.BDService;
import socialnetwork.service.UtilizatorService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Ui {
    private UtilizatorService service;
    private BDService servicebd;

    /**
     * add a user
     * @throws IOException
     */
//    private void add_utilizator() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            String elem = reader.readLine();
//            String[] sir=elem.split(" ");
//            if(sir.length<3)
//                throw new IllegalArgumentException("The number of arguments is invalid!");
//
//            Utilizator user = new Utilizator(sir[1],sir[2]);
//            user.setId(Long.parseLong(sir[0]));
//            service.addUtilizator(user);
//        }
//        catch (ValidationException | IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }

    private void add_user_bd() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String elem = reader.readLine();
            String[] sir=elem.split(" ");
            if(sir.length<2)
                throw new IllegalArgumentException("The number of arguments is invalid!");

            Utilizator user = new Utilizator(sir[0],sir[1],sir[2]);
            //user.setId(Long.parseLong(sir[0]));
            servicebd.add_user(user);
        }
        catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
        catch (IllegalArgumentException et) {
            System.out.println(et.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * remove a user
     * @throws IOException
     */
//    private void delete_user() throws IOException {
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            Long id = Long.parseLong(reader.readLine());
//            service.deleteUtilizator(id);
//        } catch (IOException e) {
//            //System.out.println("id invalid!");
//        } catch (ValidationException e) {
//            System.out.println(e.getMessage());
//        } catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }

    void delete_user_bd() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.parseLong(reader.readLine());
            servicebd.delete_user(id);
        } catch (IOException e) {
            //System.out.println("id invalid!");
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * add a friendship
     * @throws IOException
     */
//    private void add_friend() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String elem = reader.readLine();
//        String[] elem2=elem.split(" ");
//
//        try {
//            if(elem2.length!=2)
//                throw new IllegalArgumentException("The number of arguments is invalid!");
//            Long x = Long.parseLong(elem2[0]);
//            Long y = Long.parseLong(elem2[1]);
//            service.add_friend(x,y);
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        }
//
//    }

    private void add_friends_bd() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if(elem2.length!=2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            Long y = Long.parseLong(elem2[1]);
            if(x > y) {
                Long aux = x;
                x = y;
                y =aux;
            }
            servicebd.add_friend(x,y);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }

    /**
     * remove a friendship
     * @throws IOException
     */
//    private void delete_friendship() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String elem = reader.readLine();
//        String[] elem2=elem.split(" ");
//
//        try {
//            if ( elem2.length < 2)
//                throw new IllegalArgumentException("The number of arguments is invalid!");
//            Long x = Long.parseLong(elem2[0]);
//            Long y = Long.parseLong(elem2[1]);
//
//            service.delete_friendship(x,y);
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        }
//
//    }

    private void delete_friendship_bd() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length < 2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            Long y = Long.parseLong(elem2[1]);
            if(x > y) {
                Long aux = x;
                x = y;
                y =aux;
            }
            servicebd.delete_friendship(x,y);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }


    /**
     * prints all entities
     */
    private void print_all() {

        service.getAll().forEach(System.out::println);

    }

    private void cauta_prieteni() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length!= 1)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            ArrayList<String> lista = (ArrayList<String>) servicebd.afiseaza_prieteni(x);
            lista.forEach(System.out::println);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }

    private void cauta_prieteni2() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length!= 2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            String y =elem2[1];
            ArrayList<String> lista = (ArrayList<String>) servicebd.afiseaza_prieteni2(x,y);
            if(lista.isEmpty())
                System.out.println("nu exista");
            else
                lista.forEach(System.out::println);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }

//    private void add_message() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            System.out.print("from: ");
//            Long from = Long.parseLong(reader.readLine());
//
//            System.out.print("to: ");
//            String elem = reader.readLine();
//            String[] elem2 = elem.split(" ");
//            if(elem2.length<1)
//                throw new IllegalArgumentException("introduce utilizatoori");
//
//            System.out.print("message: ");
//            String message = reader.readLine();
//
//            servicebd.add_message(from,message,elem2);
//
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        }
//
//
//    }
//
//    public void add_reply_one() {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            System.out.print("id message reply: ");
//            Long id_message_reply = Long.parseLong(reader.readLine());
//
//            System.out.print("from: ");
//            Long from = Long.parseLong(reader.readLine());
//
//            System.out.print("to: ");
//            Long to = Long.parseLong(reader.readLine());
//
//            System.out.println("message: ");
//            String msg = reader.readLine();
//
//            servicebd.add_reply_one(id_message_reply,from,to,msg);
//
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void add_reply_all() {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            System.out.print("id message reply: ");
//            Long id_message_reply = Long.parseLong(reader.readLine());
//
//            System.out.print("from: ");
//            Long from = Long.parseLong(reader.readLine());
//
//            System.out.println("message: ");
//            String msg = reader.readLine();
//
//            servicebd.add_reply_all(id_message_reply,from,msg);
//
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    void add_friendrequest() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Intorduce from(Long) to(Long): ");
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length < 2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            Long y = Long.parseLong(elem2[1]);

            servicebd.add_service_friendrequest(x,y);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }

    }

//    public void print_conversatie() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String elem = reader.readLine();
//        String[] elem2=elem.split(" ");
//
//        try {
//            if ( elem2.length!=2)
//                throw new IllegalArgumentException("The number of arguments is invalid!");
//            Long x = Long.parseLong(elem2[0]);
//            Long y = Long.parseLong(elem2[1]);
//
//            servicebd.print_conversatie(x,y).forEach(System.out::println);
//
//        }catch(IllegalArgumentException  e) {
//            System.out.println(e.getMessage());
//        } catch (ValidationException et) {
//            System.out.println(et.getMessage());
//        }
//
//    }

    public void accept_friendrequest() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length < 2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            Long y = Long.parseLong(elem2[1]);

            servicebd.service_accept_friendrequest(x,y);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }

    public void delete_friendrequest() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String elem = reader.readLine();
        String[] elem2=elem.split(" ");

        try {
            if ( elem2.length < 2)
                throw new IllegalArgumentException("The number of arguments is invalid!");
            Long x = Long.parseLong(elem2[0]);
            Long y = Long.parseLong(elem2[1]);

            servicebd.service_delete_friendrequest(x,y);

        }catch(IllegalArgumentException  e) {
            System.out.println(e.getMessage());
        } catch (ValidationException et) {
            System.out.println(et.getMessage());
        }
    }

    public Ui(UtilizatorService service, BDService servicebd) {
        this.service = service;
        this.servicebd=servicebd;
      //  this.servicemessage = servicemsg;
    }

    public void run() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        while (true){
           // System.out.println("1.ADD User\n2.DELETE User\n3.ADD Friedships\n4.DELETE Friendships\n5.ALL Users");
            //System.out.println("11.ADD User\n12.DELETE User\n13.ADD Friedships\n14.DELETE Friendships\n15.ALL Users\n16.ALL Friendships");
            //System.out.println("21.NR de componente conexe\n22.Cel mai lung drum");
            System.out.println("31.Friends of user\n32....\nsend_m->send a message\nreply_one\nreply_all\nadd_fr->add friend request");
            System.out.print("Introduce comanda: ");
            String cmd=reader.readLine();
            switch (cmd){
                case "11":
                    System.out.print("Introduce(nume prenume password): ");
                    add_user_bd();
                    break;
                case "12":
                    System.out.print("Introduce(id): ");
                    delete_user_bd();
                    break;
                case "5":
                    print_all();
                    break;
                case "15":
                    servicebd.getAll().forEach(System.out::println);
                    break;
                case "16":
                    servicebd.getAll2().forEach(System.out::println);
                    break;
                case "13":
                    System.out.print("Introduce(id1 id2): ");
                    add_friends_bd();
                    break;
                case "14":
                    System.out.print("Introduce(id1 id2): ");
                    delete_friendship_bd();
                    break;
                case "21":
                    System.out.print("nr de elem conexe ");
                    System.out.println(service.find_number_of_conex_elem());
                    break;
                case "22":
                    System.out.print("drum: ");
                    System.out.println(service.cel_mai_lung_drum());
                    break;
                case "31":
                    System.out.println("Introduce(id): ");
                    cauta_prieteni();
                    break;
                case "32":
                    System.out.println("Introduce(id luna): ");
                    cauta_prieteni2();
                    break;
//                case "send_m":
//                    add_message();
//                    break;
//                case "reply_one":
//                    add_reply_one();
//                    break;
//                case "reply_all":
//                    add_reply_all();
//                    break;
//                case "print conv":
//                    System.out.print("Introduce id1 id2: ");
//                    print_conversatie();
//                    break;
                case "add_fr":
                    add_friendrequest();
                    break;
                case "accept_fr":
                    System.out.print("Introduce to(Long) from(Long): ");
                    accept_friendrequest();
                    break;
                case "delete_fr":
                    System.out.print("Introduce to(Long) from(Long): ");
                    delete_friendrequest();
                    break;
                default:
                    System.out.println("comanda invalida!");
                    break;
            }


        }
    }
}
