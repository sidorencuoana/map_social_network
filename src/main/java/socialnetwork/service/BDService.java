package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.EventBDRepository;
import socialnetwork.repository.database.FriendRequestBDRepository;
import socialnetwork.repository.database.MessageBDRepository;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.TaskChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class BDService implements Observable<TaskChangeEvent> {
    private final Repository<Long, Utilizator> repobd;
    private final Repository<Tuple<Long,Long>, Prietenie> repobd_friends;
    private final MessageBDRepository repo_message;
    private final EventBDRepository repo_event;
    private final FriendRequestBDRepository repo_fr;
    private List<Observer<TaskChangeEvent>> observers = new ArrayList<>();

    public BDService(Repository<Long, Utilizator> repobd,Repository<Tuple<Long,Long>, Prietenie> repobd_friends,MessageBDRepository repo_message,FriendRequestBDRepository repo_fr,EventBDRepository repo_event) {

        this.repobd = repobd;
        this.repobd_friends = repobd_friends;
        this.repo_message = repo_message;
        this.repo_fr = repo_fr;
        this.repo_event = repo_event;
    }

    public Iterable<Utilizator> getAll(){

        return repobd.findAll();
    }

    public Iterable<Prietenie> getAll2() {
        return repobd_friends.findAll();
    }


    public Utilizator find_user(Long id) {
        return repobd.findOne(id);
    }

    public Utilizator add_user(Utilizator user) {
        return repobd.save(user);
    }

    public Utilizator delete_user(Long id) throws IOException {
        Utilizator user = repobd.delete(id);
        return user;
    }

    public Prietenie add_friend(Long x,Long y) {
        repobd.findOne(x);
        repobd.findOne(y);
        Tuple<Long,Long> tru = new Tuple<>(x,y);
        Prietenie prietenie = repobd_friends.findOne(tru);
        if(prietenie != null)
            throw new ValidationException("prietenie existenta!");
        Prietenie pri = new Prietenie(LocalDateTime.now());
        pri.setId(tru);
        return repobd_friends.save(pri);
    }

    public Prietenie delete_friendship(Long x,Long y) throws IOException {
        Tuple<Long,Long> tru = new Tuple<>(x,y);
        Prietenie prietenie = repobd_friends.findOne(tru);
        if(prietenie == null)
            throw new ValidationException("prietenia nu exista!");
        repobd_friends.delete(tru);
        return null;
    }

    public Iterable<String> afiseaza_prieteni(Long x) {
        Utilizator user = repobd.findOne(x);
        ArrayList<String> list = new ArrayList<>();
        StreamSupport.stream(repobd_friends.findAll().spliterator(), false)
                .filter(y->{
                   // System.out.println(y.toString());
                    return y.getId().getRight()==x || y.getId().getLeft()==x;
                })
                .forEach(y->{
                    Utilizator u;
                    if(y.getId().getLeft() == x){
                        u = repobd.findOne(y.getId().getRight());
                    }
                    else
                        u = repobd.findOne(y.getId().getLeft());
                    String string = new String(u.getFirstName() + " " + u.getLastName() + " " + y.getDate());
                    list.add(string);
                });
            return list;

    }

    public Iterable<String> afiseaza_prieteni2(Long x,String luna) {
        Utilizator user = repobd.findOne(x);
        ArrayList<String> list = new ArrayList<>();
        StreamSupport.stream(repobd_friends.findAll().spliterator(), false)
                .filter(y->{
                    // System.out.println(y.toString());
                    return y.getId().getRight()==x || y.getId().getLeft()==x;
                })
                .filter(y->y.getDate().toString().contains("-"+luna+"-"))
                .forEach(y->{
                    Utilizator u;
                    if(y.getId().getLeft() == x){
                        u = repobd.findOne(y.getId().getRight());
                    }
                    else
                        u = repobd.findOne(y.getId().getLeft());
                    String string = new String(u.getFirstName() + " " + u.getLastName() + " " + y.getDate());
                    list.add(string);
                });
        return list;

    }

    public Long add_message (Long from,String message, Long to,Long reply,Long id_grup) {
        Message msg = new Message(from,message,to,LocalDateTime.now(), reply,id_grup);
        return repo_message.save_message(msg);
      //  notifyObservers(new TaskChangeEvent(ChangeEventType.SEND_PRIVATE_MESSAGE));

    }
//
//    public void add_reply_one(Long id_message_reply,Long from,Long to,String message) {
//        repobd.findOne(from);
//        repobd.findOne(to);
//        Message msg3 = repo_message.find_message(id_message_reply);
//
//        int ok=0,ok1=0;
//        for (Long ele: repo_message.find_all_to(id_message_reply)) {
//            if (ele == from)
//                ok = 1;
//            if(ele==to)
//                ok1=1;
//        }
//        if(to==msg3.getFrom())
//            ok1++;
//
//        if(ok==0 || ok1==0)
//            throw new ValidationException("error");
//
//        List<Long> to_list  = new ArrayList<>();
//        to_list.add(to);
//
//        Message msg = new Message(from,message,to_list,LocalDateTime.now(),id_message_reply);
//        repo_message.save_message(msg);
//
//    }
//
//    public void add_reply_all(Long id_message_reply,Long from,String message) {
//        repobd.findOne(from);
//        Message mes = repo_message.find_message(id_message_reply);
//
//        int ok=0;
//        List<Long> to_list  = new ArrayList<>();
//        for(Long ele: repo_message.find_all_to(id_message_reply)) {
//            if(from!=ele)
//                to_list.add(ele);
//            if(from==ele)
//                ok=1;
//        }
//        to_list.add(mes.getFrom());
//
//        if(ok==0)
//            throw new ValidationException("error");
//
//        Message msg = new Message(from,message,to_list,LocalDateTime.now(),id_message_reply);
//        repo_message.save_message(msg);
//
//    }
//
//    public List<Message> print_conversatie(Long id1,Long id2) {
//       List<Message> list = new ArrayList<>();
//       repobd.findOne(id1);
//       repobd.findOne(id2);
//
//       for(Message msg:repo_message.findAll(id1,id2)) {
//           for(Long ele:msg.getTo()) {
//               if(msg.getFrom()==id1 && id2==ele)
//                   list.add(msg);
//               if(msg.getFrom()==id2 && id1==ele)
//                   list.add(msg);
//           }
//       }
//
//       if(list.size()<2)
//               throw new ValidationException("Nu exista conversatie!");
//       return list;
//
//
//    }



    public void add_service_friendrequest(Long from,Long to) {
        //verificam daca exista useri
        repobd.findOne(from);
        repobd.findOne(to);
        //verific daca exista prietenia
        Tuple<Long,Long> tru,tru2;
        if(from>to) {
            tru = new Tuple<>(to, from);
        }
        else {
            tru = new Tuple<>(from,to);
        }
        if(repobd_friends.findOne(tru)!=null) {
            throw new ValidationException("Esti deja prieten cu utilizatorul ales!");
        }

        tru = new Tuple<>(from,to);
        //verificam daca exista cererea
        tru2 = new Tuple<>(to,from);
        FriendRequest pri = repo_fr.findOne(tru);
        FriendRequest pri2 = repo_fr.findOne(tru2);
        if(pri==null && pri2==null)
        {
            FriendRequest fr = new FriendRequest("PENDING",LocalDateTime.now());
            fr.setId(tru);

            repo_fr.save(fr);
            notifyObservers(new TaskChangeEvent(ChangeEventType.FRIEND_REQUEST));
        }
        else
        if(pri2!=null && !pri2.getStatus().equals("REJECTED"))
        {
            throw new ValidationException("Exista o cerere trimisa pentru dine de la utilizatorul selectat");
        }
        else
        if(pri2!=null && pri2.getStatus().equals("REJECTED"))
        {
            repo_fr.delete(tru2);
            FriendRequest fr = new FriendRequest("PENDING",LocalDateTime.now());
            fr.setId(tru);

            repo_fr.save(fr);
            notifyObservers(new TaskChangeEvent(ChangeEventType.FRIEND_REQUEST));
        }
        else
        if(pri.getStatus().equals("PENDING") || pri.getStatus().equals("ACCEPTED")) {
            throw new ValidationException("Exista o cerere deja trimisa!");
        }
        else
        if(pri.getStatus().equals("REJECTED")) {
            repo_fr.delete(tru);
            FriendRequest fr = new FriendRequest("PENDING",LocalDateTime.now());
            fr.setId(tru);
            repo_fr.save(fr);
            notifyObservers(new TaskChangeEvent(ChangeEventType.FRIEND_REQUEST));

        }



    }

    public void service_accept_friendrequest(Long to, Long from) {
        //verificam daca from si to exista
        repobd.findOne(from);
        repobd.findOne(to);

        //verificam daca cererea exista
        Tuple<Long,Long> tru = new Tuple<>(from,to);
        FriendRequest prietenie = repo_fr.findOne(tru);
        if(prietenie!=null && prietenie.getStatus().equals("PENDING"))
        {
            Tuple<Long,Long> tru2 = null;
            if(from>to) {
                tru2 = new Tuple<>(to,from);
            }
            else
            {
                tru2 = new Tuple<>(from,to);
            }
            Prietenie pri = new Prietenie(LocalDateTime.now());
            pri.setId(tru2);
            repobd_friends.save(pri);

            repo_fr.delete(tru);

            prietenie.setStatus("APPROVED");
            prietenie.setData_time(LocalDateTime.now());
            repo_fr.save(prietenie);

            Tuple<Long,Long> tru1 = new Tuple<>(to,from);
            FriendRequest prietenie1 = repo_fr.findOne(tru1);
            if(prietenie1!=null)
            {
                repo_fr.delete(tru1);
            }

            notifyObservers(new TaskChangeEvent(ChangeEventType.ADD_FRIEND));

        }
        else
        {
            throw new ValidationException("cererea de pretenie nu exista!");
        }


    }

    public void service_delete_friendrequest(Long to,Long from) {
        repobd.findOne(from);
        repobd.findOne(to);

        Tuple<Long,Long> tru = new Tuple<>(from,to);
        FriendRequest prietenie = repo_fr.findOne(tru);
        if(prietenie!= null && prietenie.getStatus().equals("PENDING"))
        {
            repo_fr.delete(tru);
            prietenie.setData_time(LocalDateTime.now());
            prietenie.setStatus("REJECTED");
            repo_fr.save(prietenie);
            notifyObservers(new TaskChangeEvent(ChangeEventType.DELETE_FRIEND_REQUEST));
        }
        else
        {
            throw new ValidationException("Cererea nu exista!");
        }



    }


    @Override
    public void addObserver(Observer<TaskChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void notifyObservers(TaskChangeEvent t) {
        observers.forEach(x->x.update(t));
    }

    public boolean sunt_prieteni(Long from,Long to)
    {
        Tuple<Long,Long> tru;
        if(from>to) {
            tru = new Tuple<>(to, from);
        }
        else {
            tru = new Tuple<>(from,to);
        }
        if(repobd_friends.findOne(tru)!=null) {
            return true;
        }
        return false;
    }

    public boolean exista_cerere_pending_from_to(Long to,Long from)
    {
        Tuple<Long,Long> tru = new Tuple<>(to,from);
        FriendRequest fr = repo_fr.findOne(tru);
        if(fr == null) {
           return false;
        }
        if(fr!=null && fr.getStatus().equals("PENDING")) {
            return true;
        }
        return false;
    }

    public void retragere_cerere (Long to,Long from) {
        //verifica daca exista cererea si daca este pennding
        Tuple<Long,Long> tru = new Tuple<>(to,from);
        FriendRequest fr = repo_fr.findOne(tru);
        if(fr == null) {
            throw new ValidationException("Nu exista friend request pentru utilizatorul selectat!");
        }
        if(fr!=null && fr.getStatus().equals("PENDING")) {
            repo_fr.delete(tru);
        }
        else
        {
            throw new ValidationException("Cererea de prietenie nu mai poate sa fie retrasa!");
        }

        //daca da atunci o sterg din bd
        //daca nu arunc eroare
    }

    public List<Long> get_all_friends(Long id) {
        return repobd_friends.all_friends(id);
    }

    public List<FriendRequest> get_all_friendrequest(Long id) {return repo_fr.find_all_friendrequest(id);}

    public void delete_FE(Long from,Long to) {
        Tuple<Long,Long> tru = new Tuple<>(from,to);
        FriendRequest fr = repo_fr.findOne(tru);
        if(fr!=null) {
            repo_fr.delete(tru);
        }
        else
        {
           tru = new Tuple<>(to,from);
            repo_fr.delete(tru);
        }

    }

    public List<Long> get_users_id_chat(Long id) {
        return repo_message.return_all_private_chats_user(id);
    }

    public List<Message> get_conversation(Long id1,Long id2,Long count) {
        return repo_message.find_conversation(id1,id2,count);
    }

    public Message get_message(Long id) {
        return repo_message.find_message(id);
    }

    public void create_group(String nume,List<Long> list) {
        repo_message.save_group(nume,list);
        notifyObservers(new TaskChangeEvent(ChangeEventType.CREATE_GROUP));

    }

    public List<Group> get_All_groups(Long id) {
        return repo_message.get_all_groups(id);
    }

    public List<Message> get_chat_grup(Long id,Long count) {
        return repo_message.get_all_message_group(id,count);
    }

    public List<PDF1> get_all_message_for(Long id, LocalDate start,LocalDate end) {
        return repo_message.find_all_to(id)
                .stream()
                .filter(x->{
                    String yy = x.getData().toString();
                    String xx = yy.substring(0,Math.min(yy.length(),10));
                    return xx.compareTo(start.toString()) >= 0 && xx.compareTo(end.toString()) < 0;
                })
                .map(x->{
                        Utilizator userr = repobd.findOne(x.getFrom());
                        String str = userr.getFirstName() +" "+ userr.getLastName()+ "\n" + x.getData().toString();
                        return new PDF1(str,x.getMessage());

                })
                .collect(Collectors.toList());
    }

    public List<PDF1> get_all_message_from_one_user(Long id_from,Long id_to,LocalDate start,LocalDate end) {
        return repo_message.get_all_message_from_one_user(id_from,id_to)
                .stream()
                .filter(x->{
                    String yy = x.getData().toString();
                    String xx = yy.substring(0,Math.min(yy.length(),10));
                    return xx.compareTo(start.toString()) >= 0 && xx.compareTo(end.toString()) < 0;
                })
                .map(x->{
                    Utilizator userr = repobd.findOne(x.getFrom());
                    String str = userr.getFirstName() +" "+ userr.getLastName()+ "\n" + x.getData().toString();
                    return new PDF1(str,x.getMessage());

                })
                .collect(Collectors.toList());

    }

    public List<Message> get_all_message_user(Long id) {
        return repo_message.get_all_message_for_one_user(id);
    }

    public List<Message> get_all_message_grup(Long id) {return repo_message.get_all_grup_message_from_one_user(id);}

    public List<Event> get_all_events(Long count) {return repo_event.get_all_events(count);}

    public void save_event(Event ev) {
        repo_event.save(ev);
        notifyObservers(new TaskChangeEvent(ChangeEventType.ADD_EVENT));
    }

    public List<Event> get_all_user_event(Long id,Long count) {
        return repo_event.get_all_event_one_user(id,count);
    }

    public void update_notify(Long id_u,Long id_e,Long value) {
        repo_event.update_notify(id_u,id_e,value);
        notifyObservers(new TaskChangeEvent(ChangeEventType.NOTIFY));
    }

    public void user_join_event(Long id_e,Long id_u){
        repo_event.join_user_event(id_e,id_u);
        notifyObservers(new TaskChangeEvent(ChangeEventType.JOIN_EVENT));
    }

    public void user_leave_event(Long id_e,Long id_u){
        repo_event.leave_user_event(id_e,id_u);
        notifyObservers(new TaskChangeEvent(ChangeEventType.LEAVE_EVENT));
    }

    public Long number_user() {
        return repobd.size();

    }

    public List<Utilizator> get_all_users(Long count,String nume){
        return repobd.findAll2(count,nume);

    }

    public Long size_event(){
        return repo_event.size();
    }

    public Long size_my_events(Long id)
    {
       return repo_event.size_user_event(id);
    }

    public Long size_conversation_users(Long id1,Long id2)
    {
        return repo_message.size_conversation_2_users(id1,id2);
    }

    public Long size_conversation_group(Long id)
    {
        return repo_message.size_message_group(id);
    }

    public List<Event> get_all_my_events(Long id)
    {
        return repo_event.get_all_event_one_user2(id);
    }
}
