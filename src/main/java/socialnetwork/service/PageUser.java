package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Message;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.TaskChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PageUser implements Observable<TaskChangeEvent> {
    private final Long id;
    private final String nume;
    private final String prenume;
    private List<Long> list_friends;
    private List<FriendRequest> list_friendsRequrest;
    private final List<Message> list_private_message;
    private final List<Message> list_grup_message;
    private  List<Event> list_event;
    private final List<Observer<TaskChangeEvent>> observers = new ArrayList<>();

    public PageUser(Long id,String nume, String prenume,List<Long> list_friends,List<FriendRequest> list_friendsRequrest,List<Message> list_private_message
                   ,List<Message> list_grup_message,List<Event> list_event) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.list_friends = list_friends;
        this.list_friendsRequrest = list_friendsRequrest;
        this.list_private_message = list_private_message;
        this.list_grup_message = list_grup_message;
        this.list_event = list_event;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public List<Long> getList_friends() {
        return list_friends;
    }

    public List<FriendRequest> getList_friendsRequrest() {
        return list_friendsRequrest;
    }

    public void setList_friendsRequrest(List<FriendRequest> list_friendsRequrest) {
        this.list_friendsRequrest = list_friendsRequrest;
    }

    public void setList_friends(List<Long> list_friends) {

        this.list_friends = list_friends;
    }

    public Long getId() {
        return id;
    }

    public List<Event> getList_event() {
        return list_event;
    }


    public void setList_event(List<Event> list_event) {
        this.list_event = list_event;
    }

    public List<Message> getConversation(Long id1) {
        return list_private_message
                .stream()
                .filter(x-> ((x.getTo().equals(id1) && x.getFrom().equals(id)) || (x.getTo().equals(id) && x.getFrom().equals(id1))))
                .collect(Collectors.toList());
    }

    public void delete_friend(List<Long> list_friends2) {
        setList_friends(list_friends2);
        notifyObservers(new TaskChangeEvent(ChangeEventType.DELETE_FRIEND));
    }

    public void delete_FR(List<FriendRequest> list_friendsRequrest2)
    {
        setList_friendsRequrest(list_friendsRequrest2);
        notifyObservers(new TaskChangeEvent(ChangeEventType.DELETE_FRIEND_REQUEST));
    }

    public void retragere_FR(List<FriendRequest> list_friendsRequrest2) {
        setList_friendsRequrest(list_friendsRequrest2);

    }

    public void accept_friend_request(List<Long> list1,List<FriendRequest> list2) {
        setList_friends(list1);
        setList_friendsRequrest(list2);
        notifyObservers(new TaskChangeEvent(ChangeEventType.ADD_FRIEND));
    }

    public void decline_friend_request(List<FriendRequest> list2)
    {
        setList_friendsRequrest(list2);
        notifyObservers(new TaskChangeEvent(ChangeEventType.DELETE_FRIEND_REQUEST));
    }

    public void add_message(Long id,Long from,String message, Long to,Long reply,Long id_grup) {
        Message msg = new Message(id,from,to,message, LocalDateTime.now(), reply,id_grup);
        list_private_message.add(msg);
        notifyObservers(new TaskChangeEvent(ChangeEventType.SEND_PRIVATE_MESSAGE));

    }

    public List<Message> get_chat_grup(Long id) {
        return list_grup_message
                .stream()
                .filter(x->(x.getId_grup().equals(id)))
                .collect(Collectors.toList());

    }

    public void add_message_grup(Long idd,Long from,String message, Long to,Long reply,Long id_grup) {
        Message msg = new Message(idd,from,to,message, LocalDateTime.now(), reply,id_grup);
        list_grup_message.add(msg);
        notifyObservers(new TaskChangeEvent(ChangeEventType.SEND_PRIVATE_MESSAGE));


    }

    public boolean user_particip_event(Long idd){
        for(Event ele:list_event) {
            if (ele.getId().equals(idd)) {
                return true;
            }
        }
        return false;
    }

    public boolean stare_user_event(Long idd){
        for(Event ele:list_event) {
            if (ele.getId().equals(idd) && ele.getNotify() == 1) {
                return true;
            }
            if(ele.getId().equals(idd) && ele.getNotify() == 0) {
                return false;
            }
        }
        return false;
    }

    public void user_participa_la_event_notifica_0(Long idd) {
        for(Event ele:list_event) {
            if(ele.getId().equals(idd) && ele.getNotify() == 0) {
                ele.setNotify((long) 1);
            }

        }
    }

    public void user_participa_la_event_notifica_1(Long idd) {
        for(Event ele:list_event) {
            if(ele.getId().equals(idd) && ele.getNotify() == 1) {
                ele.setNotify((long) 0);
            }
        }
    }

    public void user_join_event(Event event) {
        Event newevent = new Event(event.getId(),event.getNume(),event.getDescriere(),event.getData_start(),event.getData_end(),event.getTime_start(),event.getTime_end(), (long) 1);
        list_event.add(newevent);
        notifyObservers(new TaskChangeEvent(ChangeEventType.JOIN_EVENT));
    }



    public void user_leave_event(Event event,List<Event> list) {
        setList_event(list);
        notifyObservers(new TaskChangeEvent(ChangeEventType.LEAVE_EVENT));
    }

    @Override
    public void addObserver(Observer<TaskChangeEvent> e) {
        observers.add(e);

    }

    @Override
    public void notifyObservers(TaskChangeEvent t) {
        observers.forEach(x->x.update(t));

    }
}
