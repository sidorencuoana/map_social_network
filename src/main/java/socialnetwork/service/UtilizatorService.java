package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UtilizatorService {
    private final Repository<Long, Utilizator> repo;
    private final Repository<Tuple<Long,Long>, Prietenie> repo_friends;


    public UtilizatorService(Repository<Long, Utilizator> repo, Repository<Tuple<Long,Long>,Prietenie> repo_friends) {
        this.repo = repo;
        this.repo_friends = repo_friends;
        repo_friends.findAll().forEach(this::load_friends);
    }

    public Utilizator addUtilizator(Utilizator messageTask) {
        return repo.save(messageTask);
    }

    /**
     * remove a user
     * @param id-user id
     * @return user-ul sters
     * @throws IOException-
     */
    public Utilizator deleteUtilizator(Long id) throws IOException {
        repo.findOne(id);
        List<Long> friends = repo.all_friends(id);
        repo.delete(id);
        for (Long elem:friends) {
            Tuple<Long, Long> tru = new Tuple<>(id, elem);
            //Prietenie pri = repo_friends.findOne(tru);
            repo_friends.delete(tru);
            repo.remove_friend(elem,id);
        }
        return null;
    }

    /**
     * add a friendship between the user with id=x and the user with id=y
     * @param x-id from user1
     * @param y-id from user2
     * @return prietenie dintre x si y
     */
    public Prietenie add_friend(Long x,Long y) {
        repo.findOne(x);
        repo.findOne(y);
        Prietenie pri = new Prietenie(LocalDateTime.now());
        Tuple<Long,Long> tru = new Tuple<>(x,y);
        pri.setId(tru);
        Prietenie elem = repo_friends.save(pri);
        repo.add_friends(x,y);
        return elem;
    }

    /**
     * remove friendship between the user with id=x and the user with id=y
     * @param x-id from user1
     * @param y-od from user2
     * @return Prietenie
     * @throws IOException
     */
    public Prietenie delete_friendship(Long x,Long y) throws IOException {
        Tuple<Long,Long> tru = new Tuple<Long,Long>(x,y);
        repo_friends.findOne(tru);
        repo.remove_friend(x,y);
        repo.remove_friend(y,x);
        return repo_friends.delete(tru);
    }

    /**
     * @return all users
     */
    public Iterable<Utilizator> getAll(){

        return repo.findAll();
    }


    /**
     * apeleaza functia add_friends
     * @param e-Prietenie
     */
    public void load_friends(Prietenie e) {
        Tuple<Long, Long> id = e.getId();
        Long id1 = id.getLeft();
        Long id2 = id.getRight();
        repo.add_friends(id1,id2);
    }


    /**
     * DFS(X)
     * @param x-Utilizator
     * @param vizitat-map de vizitat
     * @param distanta-map de distanta
     * @param time-distanta
     */
    public void dfs(Utilizator x, Map<Long,Long> vizitat, Map<Long,Long>  distanta, Long time) {
        time++;
        distanta.replace(x.getId(),time);
        vizitat.replace(x.getId(), (long) 1);
        for(Long ele:x.getFriends()) {
            if(vizitat.get(ele) == 0) {
                Utilizator user = repo.findOne(ele);
                dfs(user,vizitat,distanta,time);
            }
        }
    }

    /**
     *
     * @return numarul de elemente conexe
     */
    public Integer find_number_of_conex_elem(){
        Map<Long,Long>  distanta = new HashMap<>();
        Map<Long,Long>  vizitat = new HashMap<>();
        for(Utilizator ele:repo.findAll())
        {
            distanta.put(ele.getId(), (long) 0);
            vizitat.put(ele.getId(), (long) 0);
        }
        Integer conex = 0;
        for(Utilizator ele:repo.findAll()) {
            if (vizitat.get(ele.getId()) == 0)
            {
                dfs(ele,vizitat,distanta, (long) 0);
                conex++;
            }
        }
        System.out.println(distanta);
        return conex;


    }

    /**
     *
     * @return cel mai lung drum
     */
    public Integer cel_mai_lung_drum(){
        Map<Long,Long>  distanta = new HashMap<>();
        Map<Long,Long>  vizitat = new HashMap<>();
        for(Utilizator ele:repo.findAll())
        {
            distanta.put(ele.getId(), (long) 0);
            vizitat.put(ele.getId(), (long) 0);
        }
        Integer max=0;
        Utilizator user1 = null;
        for(Utilizator ele:repo.findAll()) {
                dfs(ele,vizitat,distanta, (long) -1);
                for(Utilizator user:repo.findAll()) {
                    Long id = user.getId();
                    if(distanta.get(id)>max) {
                        max= Math.toIntExact(distanta.get(id));
                        user1 = repo.findOne(id);
                    }
                }
            for(Utilizator elems:repo.findAll()) {
                vizitat.replace(elems.getId(), (long) 0);
                distanta.replace(elems.getId(), (long) 0);
            }
        }
//        dfs(user1,vizitat,distanta, (long) 0);
//        List<Utilizator> lista = new ArrayList<>();
//        Set<Map.Entry<Long,Long>> set = distanta.entrySet();
//        for(var elems:set) {
//            if(elems.getValue()!=0)
//                System.out.println(repo.findOne(elems.getKey()));
//        }
//      //  System.out.println(distanta);

        return max;


    }



}
